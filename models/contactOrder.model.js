const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const contactOrderSchema = new Schema({
    // name: 'Name:',
    name: { type: String, require: true },

    //email: 'Email:',
    email: { type: String, require: false },

    // phoneNumber: 'Phone Number:',
    phoneNumber: { type: String, require: false },

    //subject: 'Subject:',
    subject: { type: String, require: false },

    // wordCount: 'Word Count:',
    wordCount: { type: Number, require: false },

    // major: 'Major:',  
    major: { type: String, require: false },

    //institution: 'Institution:',
    institution: { type: String, require: false },

    // dueDate: 'Due date:',
    dueDate: { type: Date, require: false, default: null },

    // file
    files: [{ type: mongoose.Types.ObjectId, ref: "file" }],

    createdAt: { type: Date, require: true, default: new Date() },

    modifiedAt: { type: Date, require: true, default: new Date() },
});

const CONTACT_ORDER_MODEL = mongoose.model("contact_order", contactOrderSchema);

module.exports = CONTACT_ORDER_MODEL;
