var express = require("express");
var router = express.Router();

const controller = require("../controllers/policy.controller.js");
const { parseFormData } = require("../helpers/index.js");
const { GuardController } = require("../controllers/index.js");
const viewController = require("../controllers/view/policy.controller.js");

// // -------------------- GET DATA - RENDER VIEW ----------------------------
router.get("/policies/:id", GuardController.needLogin, viewController.renderEdit);
router.get("/policies", GuardController.needLogin, viewController.renderList);

// // -------------------- CREATE OR UPDATE ----------------------------
router.post("/policies", GuardController.needLogin, parseFormData, controller.createID);
router.post("/policies/quick-update/:id", GuardController.needLogin, parseFormData, controller.quickUpdate);
router.put('/policies/:id', GuardController.needLogin, parseFormData, controller.createOrUpdate);
router.put('/policies/:id/status/:status', GuardController.needLogin, parseFormData, controller.updatePolicyStatus);

// // -------------------- DELETE ----------------------------
router.delete("/policies/:id", GuardController.needLogin, controller.remove);

module.exports = router;

