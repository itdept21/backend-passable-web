// @ts-nocheck
const BaseCrudController = require("../baseCrud.controller");
const service = require("../../services/policyCategory.services");
const { PAGINATION, STATUS_CODE, FORMAT_ID_FN, LIST_STATUS_POST_CATE } = require("../../constants/base.constants");
const { DATE_FORMAT_FN } = require("../../helpers/dateTimeFormat");

class ViewPolicyCategoryController extends BaseCrudController {
    constructor() {
        super(service);
    }

    renderList = async (req, res, next) => {
        console.log(" ViewPolicyCategoryController list")
        const { page = 1, search = "", ...filter } = req.query;

        const url = req._parsedOriginalUrl.pathname;
        const oldQuery = req._parsedOriginalUrl.search || "?";

        const status = parseInt(filter?.status) || null

        if (page < 0) return res.sendStatus(400);

        const limit = 15;
        const skip = (page - 1) * limit;

        const queryConditions = search ? { $text: { $search: search } } : {};
        const signalGetList = await this.service.getList({
            skip, limit,
            matchConditions: { ...queryConditions, ...filter },
            populates: ['vi', 'en'],
            sort: { modifiedAt: -1, createdAt: -1 }
        });

        const signalGetCount = await this.service.count({ ...queryConditions, ...filter });

        const total = signalGetCount.data;
        const pagination = PAGINATION(total, limit, url, oldQuery);

        if (signalGetList.error) next();
        const categories = signalGetList.data;

        // list status
        const listStatus = LIST_STATUS_POST_CATE

        res.render("admin", {
            inc: "inc/admin/policy_categories",
            title: "Danh sách",
            categories,
            pagination,
            page,
            total,
            search,
            listStatus, status,
            FORMAT_ID_FN,
            DATE_FORMAT_FN,
        });
    };

    renderEdit = async (req, res, next) => {
        try {
            const { id } = req.params;
            const lang = req.query.lang || "vi";
            const switchLangs = {
                vi: `/admin/policy-categories/${id}?lang=vi`,
                en: `/admin/policy-categories/${id}?lang=en`,
            };

            const signalGet = await this.service.get({ _id: id.trim() }, ["thumbnail", "seo_image"]);
            const result = signalGet?.data || { _id: id }

            const signalGetCategories = await this.service.getList({ matchConditions: { parent: null } });
            if (signalGetCategories.error) return res.status(STATUS_CODE.SERVER_ERROR).json(signalGetCategories);
            const categories = signalGetCategories.data;

            return res.render("admin", {
                inc: "inc/admin/policy_category",
                title: "Chỉnh sửa danh mục",
                category: result,
                lang,
                switchLangs,
                categories,
                DATE_FORMAT_FN,
            });
        } catch (error) {
            return res.status(STATUS_CODE.SERVER_ERROR).json({ error: true, message: error.message });
        }
    };
}

module.exports = new ViewPolicyCategoryController();
