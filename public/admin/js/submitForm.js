// @ts-ignore
document.addEventListener("DOMContentLoaded", async (ev) => {
	const selectInput = document.querySelector(".select-search-input");

	if (selectInput)
		// @ts-ignore
		$(".select-search-input").select2();

	// ============= FORM CREATE - UPLOAD HANDLERS
	// variables
	let formArr = document.querySelectorAll(".form-horizontal");

	let contentEditors = document.querySelectorAll(".content-editor");
	let openPopupButtonArr = document.querySelectorAll(".open-modal");
	let titleContentInput = document.querySelector('[name="title"].title-content');
	let slugContentInput = document.querySelector('[name="slug"].slug-content');

	let editor = [];
	if (contentEditors)
		contentEditors.forEach((item, index) => {
			// @ts-ignore
			editor[index] = CKEDITOR.replace(`${item.id}`, {
				height: 500,
				entities: false,
				allowedContent: true,
				entities_additional: "",
				entities_greek: false,
				entities_latin: false,
				filebrowserUploadUrl: "/admin/upload-ckeditor",
				filebrowserBrowseUrl: "/admin/upload-ckeditor",
			});
		})


	const copyButtonsDiv3 = document.querySelectorAll(".copy-button-div-3");
	copyButtonsDiv3.forEach((button) => {
		button.onclick = copyTargetAndPasteDiv3;
	});

	function setDeleteEvent() {
		const removeCategoriesButtons = document.querySelectorAll(".post-remove-btn");
		removeCategoriesButtons.forEach((button) => {
			button.onclick = removeConfirmedCategory;
		});
	}

	let editor_v2 = [];
	function copyTargetAndPasteDiv3() {
		console.log(this.dataset.copy)
		const copy = document.querySelector(this.dataset.copy);
		const paste = document.querySelector(this.dataset.paste);

		const newItem = copy.cloneNode(true);
		const newID = Date.now();
		newItem.setAttribute("id", `new-${newID}`);
		newItem.querySelectorAll("input").forEach((_) => (_.value = ""));
		newItem.querySelectorAll("img").forEach((_) => (_.src = "/wp-content/noimage.jpg"));
		newItem.querySelector("#content-editor-1234567890").innerHTML = createTextareaCKEditor(newID);
		paste.appendChild(newItem);
		let contentEditors_v2 = document.querySelectorAll(".content-editor-v2")
		console.log("contentEditors_v2.length", contentEditors_v2.length)
		const _lengthContentEditors_v2 = contentEditors_v2.length - 1
		editor_v2[_lengthContentEditors_v2] = CKEDITOR.replace(`content-editor-${newID}`, {
			height: 500,
			entities: false,
			allowedContent: true,
			entities_additional: "",
			entities_greek: false,
			entities_latin: false,
			filebrowserUploadUrl: "/admin/upload-ckeditor",
			filebrowserBrowseUrl: "/admin/upload-ckeditor",
		});

		setDeleteEvent();
	}

	function removeConfirmedCategory(e) {
		const confirmed = window.confirm("Bạn có chắc chắn muốn xoá ?");
		if (confirmed) {
			this.parentElement.parentElement.remove();
		}
	}

	function createTextareaCKEditor(index) {
		return `
			<label>Answer to the question</label>
			<textarea name="childContents" class="content-editor-v2" id="content-editor-${index}"></textarea>
			<br />`;
	}

	// functions
	function createSlugFromTitle(alias) {
		let str = alias;
		if (str && str.length > 0) {
			str = str.toLowerCase();
			str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
			str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
			str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
			str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
			str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
			str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
			str = str.replace(/đ/g, "d");
			str = str.replace(
				/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g,
				" "
			);
			str = str.replace(/ + /g, " ");
			str = str.trim();
			str = str.replace(/\s+/g, "-");
		}
		return str;
	}

	function formSubmitHandler(event) {
		event.preventDefault();

		// @ts-ignore
		Swal.fire(" Đang tải ... ");

		let fd = new FormData(this);
		let that = this;

		let contentEditors = this.querySelectorAll(".content-editor");
		const dataCKEditor = []
		if (contentEditors) {
			contentEditors.forEach((item, index) => {
				dataCKEditor.push({
					name: item.getAttribute("name"),
					value: editor[index].getData()
				})
			})
		}

		let contentEditorsV2 = this.querySelectorAll(".content-editor-v2");
		if (contentEditorsV2) {
			contentEditorsV2.forEach((item, index) => {
				dataCKEditor.push({
					name: item.getAttribute("name"),
					value: editor_v2[index].getData()
				})
			})
		};
		console.log("fd.set --------", JSON.stringify({ ...fd }));


		console.log("dataCKEditor --------", JSON.stringify(dataCKEditor));

		// get name in dataCKEditor not duplicate
		let nameCKEditor = dataCKEditor.map((item) => item.name)
		const uniqueNameCKEditor = Array.from(new Set(nameCKEditor));
		console.log("nameCKEditor", JSON.stringify(uniqueNameCKEditor));

		// check trùng name -> fd.set array name
		uniqueNameCKEditor.forEach((item, index) => {
			const _value = dataCKEditor.filter((e) => e.name.toString() == item.toString());
			console.log("_value", _value)
			fd.delete(item)
			if (_value.length > 1) {
				for (var i = 0; i < _value.length; i++) {
					fd.append(`${item}[]`, _value[i].value);
				}
			} else {
				fd.append(item, _value?.[0]?.value || "")
			}
		});
		console.log("fd.set --------", fd.get("childContents[]"));

		// parse all the input, textarea tags value to an object, name is form data (fd)
		let xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function () {
			// console.log(xhr.responseText)
			// if server has been responded and status is OK
			if (xhr.readyState == 4) {
				const result = JSON.parse(xhr.responseText);
				let data = result.data;

				if (!result.error && data) {
					// @ts-ignore
					Swal.fire({
						title: "Đăng thành công",
						icon: "success"
						// @ts-ignore
					}).then((value) => {
						/* window.location.reload() */
					});
					// variables
					let categoriesList = document.querySelector('select[name="category"]');
					// functions
					let createCategoryOption = ({ _id, name }) => {
						return `<option value="${_id}">${name}</option>`;
					};

					//events
					if (data._id && data.name && categoriesList) {
						categoriesList.innerHTML += createCategoryOption(data);
					}

					setTimeout(() => {
						if (that.dataset.callbackurl && that.dataset.callbackurl !== "false") {
							console.log(that.dataset.callbackurl, typeof that.dataset.callbackurl);
							return (window.location.href = that.dataset.callbackurl);
						}
						return window.location.reload();
					}, 1000);
				} else {
					let messError = result?.message || "Hãy điền đầy đủ thông tin bắt buộc hoặc bạn không có quyền làm việc này  !"
					// @ts-ignore
					Swal.fire(
						"Đăng thất bại",
						messError,
						"warning"
					);
				}
			}
		};

		xhr.open(this.getAttribute("method"), this.getAttribute("action"), true); // current URL
		xhr.send(fd); // send as req.body to the router
	}

	//events
	if (formArr) formArr.forEach((form) => form.addEventListener("submit", formSubmitHandler));

	if (openPopupButtonArr)
		openPopupButtonArr.forEach((button) => {
			console.log(button);
			// @ts-ignore
			button.addEventListener("click", openModalPopup);
		});

	if (titleContentInput && slugContentInput) {
		titleContentInput.addEventListener("change", () => {
			// @ts-ignore
			let slug = titleContentInput.value;
			slug.toLowerCase();
			// @ts-ignore
			slugContentInput.value = createSlugFromTitle(slug).replace(/ú|ù|ũ|ủ|ụ/g, "u");
		});
	}
});

document.querySelectorAll(".button-create-empty").forEach((button) => {
	// @ts-ignore
	button.onclick = createPost;
});

document.querySelectorAll(".button-remove").forEach((button) => {
	// @ts-ignore
	button.onclick = removeApi;
});

document.querySelectorAll(".button-post").forEach((button) => {
	// @ts-ignore
	button.onclick = postApi;
});

document.querySelectorAll(".button-update").forEach((button) => {
	// @ts-ignore
	button.onclick = updateApi;
});


function createPost(e) {
	e.preventDefault();
	const xhr = new XMLHttpRequest();
	xhr.onload = (e) => {
		// @ts-ignore
		console.log(e.responseText)
		if (xhr.status < 300) {
			const docID = JSON.parse(xhr.responseText).data._id;
			const variantID = JSON.parse(xhr.responseText).data.variantID;

			if (variantID)
				window.location.href = `/admin/${this.dataset.type?.trim()}/${docID}?lang=en&variantID=${variantID}`;
			else
				window.location.href = `/admin/${this.dataset.type?.trim()}/${docID}?lang=en`;
		} else {
			alert("Khởi tạo thất bại");
		}
	};
	xhr.open("POST", this.dataset.action);
	xhr.send();
}

// @ts-ignore
function removeApi(e) {
	const { id, url } = this.dataset;
	if (id && url) {
		const ok = window.confirm("Bạn có chắc chắn muốn xoá ?");
		if (ok) {
			const xhr = new XMLHttpRequest();
			// @ts-ignore
			xhr.onload = (e) => {
				if (xhr.status < 300) {
					const result = JSON.parse(xhr.responseText)
					console.log(result.error, result.message);
					window.location.reload();
				} else {
					alert("Xoá bài viết thất bại");
				}
			};

			xhr.open("DELETE", url);
			xhr.send();
		}
	}
}

// @ts-ignore
function postApi(e) {
	const { id, url } = this.dataset;
	if (id && url) {
		const ok = window.confirm("Bạn có chắc chắn?");
		if (ok) {
			const xhr = new XMLHttpRequest();
			// @ts-ignore
			xhr.onload = (e) => {
				if (xhr.status < 300) {
					const result = JSON.parse(xhr.responseText)
					console.log(result.error, result.message);
					window.location.reload();
				} else {
					alert("Thất bại");
				}
			};

			xhr.open("POST", url);
			xhr.send();
		}
	}
}

// @ts-ignore
function updateApi(e) {
	const { id, url } = this.dataset;
	if (id && url) {
		const xhr = new XMLHttpRequest();
		// @ts-ignore
		xhr.onload = (e) => {
			if (xhr.status < 300) {
				const result = JSON.parse(xhr.responseText)
				console.log(result.error, result.message);
				// const postID = JSON.parse(xhr.responseText).data._id;
				window.location.reload();
			} else {
				alert("Cập nhật thất bại");
			}
		};

		xhr.open("PUT", url);
		xhr.send();

	}
}
