const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const pageComponentSchema = new Schema({
	name: { type: String, trim: true, required: true },
	page: { type: String, trim: true, required: true },
	lang: { type: String, trim: true, enum: ["vi", "en"], default: "en", required: true },

	title: { type: String, trim: true },
	content: { type: String, trim: true },
	description: { type: String, trim: true },

	buttonName: { type: String, trim: true },
	buttonLink: { type: String, trim: true },

	url: { type: String, trim: true },
	images: [{ type: mongoose.Types.ObjectId, ref: "image" }],

	parent: { type: mongoose.Types.ObjectId, ref: "page_component", default: null },
	child: [{ type: mongoose.Types.ObjectId, ref: "page_component", default: null }],

	serviceCategory: [{ type: mongoose.Types.ObjectId, ref: "services_category", default: null }],
	packages: [{ type: mongoose.Types.ObjectId, ref: "service", default: null }],
	faqs: [{ type: mongoose.Types.ObjectId, ref: "faq", default: null }],
	feedbacks: [{ type: mongoose.Types.ObjectId, ref: "feedback", default: null }],

	// trường mở rộng, không cố định
	opt: [{ type: Schema.Types.Mixed, trim: true, default: null }],
	opt1: { type: Schema.Types.Mixed, trim: true, default: null },
	opt2: { type: String, trim: true, default: null },
	opt3: { type: Number, trim: true, default: 0 },

	/* SEO meta */
	seo_schema: { type: String, trim: true },
	seo_canonical: { type: String, required: false },
	seo_lang: { type: String, required: false },
	seo_title: { type: String, trim: true },
	seo_description: { type: String, trim: true },
	seo_keywords: { type: String, trim: true },
	seo_redirect: { type: String, required: false },
	seo_h1: { type: String, required: false },

	createdAt: { type: Date, require: false, default: new Date() },
	createdBy: { type: String, require: false, default: null },
	modifiedAt: { type: Date, require: false, default: new Date() },
	modifiedBy: { type: String, require: false, default: null },
});

const PAGE_COMPONENT_MODEL = mongoose.model("page_component", pageComponentSchema);

module.exports = PAGE_COMPONENT_MODEL;
