const BaseService = require('./base.services');
const model = require('../models/faq.model');

class FaqService extends BaseService {
    constructor() {
        super(model);
    };

    // count child by Category
    faqByCategory = async (category) => {
        const signalGetList = await this.getList({
            matchConditions: {
                category: category,
                status: { $gte: 1 },
            },
            skip: 0, limit: 5,
            select: 'name slug'
        })
        const signalCount = await this.count({ category: category, status: { $gte: 1 }, });
        return {
            category,
            count: signalCount?.data || 0,
            list: signalGetList?.data || [],
        };
    };
}

module.exports = new FaqService();