// @ts-nocheck
const BaseCrudController = require("../baseCrud.controller");
const service = require("../../services/servicesCategory.services");
const FaqService = require("../../services/faq.services");

const {
    PAGINATION,
    STATUS_CODE,
    FORMAT_ID_FN,
} = require("../../constants/base.constants");
const { DATE_FORMAT_FN } = require("../../helpers/dateTimeFormat");
const {
    TYPE_OF_SERVICES,
    TYPE,
    BASE_PRICE,
} = require("../../constants/services.constants");

const mongoose = require('mongoose');

class ViewServicesCategoryController extends BaseCrudController {
    constructor() {
        super(service);
    }

    renderList = async (req, res, next) => {
        const { page = 1, search = "", ...filter } = req.query;

        const url = req._parsedOriginalUrl.pathname;
        const oldQuery = req._parsedOriginalUrl.search || "?";

        const parent = filter?.parent || ""
        if (!filter?.parent) {
            delete filter?.parent
        }

        if (page < 0) return res.sendStatus(400);

        const limit = 15;
        const skip = (page - 1) * limit;

        const queryConditions = search ? { $text: { $search: search } } : {};
        const signalGetList = await this.service.getList({
            skip, limit,
            matchConditions: { ...queryConditions, ...filter },
            populates: ['parent', 'seo_image'],
            sort: { modifiedAt: -1, createdAt: -1 }
        });

        const signalGetCount = await this.service.count({ ...queryConditions, ...filter });

        const total = signalGetCount.data;
        const pagination = PAGINATION(total, limit, url, oldQuery);

        if (signalGetList.error) next();
        const categories = signalGetList.data;

        const signalGetParent = await this.service.getList({
            matchConditions: {
                parent: null,
                status: { $gte: 1 },
            },
            sort: { displayOrder: 1, parent: 1, modifiedAt: -1, createdAt: -1 }
        });
        const dataParent = signalGetParent.data || []

        res.render("admin", {
            inc: "inc/admin/service_categories",
            title: "Danh sách",
            categories,
            listParent: dataParent,
            pagination,
            page,
            total,
            search,
            parent,
            FORMAT_ID_FN,
            DATE_FORMAT_FN,
        });
    };

    duplicate = async (req, res, next) => {
        const { id } = req.params;
        const signalGet = await this.service.get({ _id: id.trim() }, ["thumbnail"]);
        const result = signalGet?.data || { _id: id }

        const _id = new mongoose.Types.ObjectId();
        const _name = result.name + " [copy" + "-" + DATE_FORMAT_FN(new Date(), 'HH:mm DD/MM/YYYY') + "]";
        const _slug = result.slug + "-copy" + "-" + DATE_FORMAT_FN(new Date(), 'HH-mm-DD-MM-YYYY');

        const dataDuplicate = {
            ...result,
            name: _name,
            slug: _slug,
            _id: _id,
        };
        await this.service.createOrUpdate(
            { _id: _id },
            dataDuplicate
        );
        res.setHeader("Content-Type", "text/html");
        res.redirect('/admin/service-categories');
    }

    renderEdit = async (req, res, next) => {
        try {
            const { id } = req.params;
            const lang = req.query.lang || "vi";
            const switchLangs = {
                vi: `/admin/service-categories/${id}?lang=vi`,
                en: `/admin/service-categories/${id}?lang=en`,
            };

            const signalGet = await this.service.get({ _id: id.trim() }, ["thumbnail", 'seo_image']);
            const result = signalGet?.data || { _id: id };

            const signalGetCategories = await this.service.getList({ matchConditions: { parent: null } });
            if (signalGetCategories.error) return res.status(STATUS_CODE.SERVER_ERROR).json(signalGetCategories);
            const categories = signalGetCategories.data;

            return res.render("admin", {
                inc: "inc/admin/service_category",
                title: "Chỉnh sửa danh mục",
                category: {
                    ...result,
                    parent: result?.parent || "",
                },
                lang,
                switchLangs,
                categories,
                DATE_FORMAT_FN,
                TYPE_OF_SERVICES,
                TYPE,
                BASE_PRICE,
            });
        } catch (error) {
            return res.status(STATUS_CODE.SERVER_ERROR).json({ error: true, message: error.message });
        }
    };
}

module.exports = new ViewServicesCategoryController();
