// @ts-nocheck
const BaseCrudController = require("./baseCrud.controller");
const service = require("../services/faqCategory.services");
const {
	STATUS_CODE,
	MESSAGES,
	RESPONSE_DATA,
} = require("../constants/base.constants");

class FaqCategoryController extends BaseCrudController {
	constructor() {
		super(service);
	}

	createOrUpdate = async (req, res, next) => {
		try {
			let id = req.params.id.trim();
			const dataUpdate = req.body;

			if (!dataUpdate?.createdAt) dataUpdate.createdAt = new Date();
			if (!dataUpdate?.createdBy) dataUpdate.createdBy = req.user.username;
			dataUpdate.modifiedBy = req.user.username;

			dataUpdate['thumbnail'] = dataUpdate['thumbnail'] ? dataUpdate['thumbnail'] : null;
			dataUpdate['seo_image'] = dataUpdate['seo_image'] ? dataUpdate['seo_image'] : null;

			const signalUpdate = await this.service.createOrUpdate({ _id: id }, dataUpdate);
			if (signalUpdate.error) return res.status(STATUS_CODE.SERVER_ERROR).json(signalUpdate);
			let faqCategory = signalUpdate.data;

			const responseJson = RESPONSE_DATA(
				{
					data: faqCategory,
					message: MESSAGES.CREATED_SUCCEED
				}
			);
			return res.status(STATUS_CODE.OK).json(responseJson);
		} catch (error) {
			return res.status(400).send(RESPONSE_DATA({ error: true, message: error.message }));
		}
	};

	updateStatus = async (req, res, next) => {
		let { id, status } = req.params;
		status = parseInt(status);
		const modifiedBy = req.user.username;

		if ([0, 1, 2].indexOf(parseInt(status)) < 0)
			return res.status(STATUS_CODE.BAD_REQUEST).json(RESPONSE_DATA({ error: true, message: "invalid_status" }));

		const signalUpdate = await this.service.update({ _id: id }, { status, modifiedBy });

		if (signalUpdate.error) return res.status(STATUS_CODE.SERVER_ERROR).json(signalUpdate);

		return res.status(STATUS_CODE.OK).json(signalUpdate);
	};

	quickUpdate = async (req, res, next) => {
		const { id } = req.params;
		const { name, display, displayOrder, publishedAt, updatedDay, seo_title, seo_description } = req.body;
		const modifiedBy = req.user.username;

		const signalGet = await this.service.get({ _id: id });

		const faq = signalGet.data;

		if (!faq) return res.status(400).json(signalGet)
		// quick update
		const signalQuickUpdate = await this.service.update(
			{
				_id: id
			},
			{
				name,
				display,
				displayOrder,
				publishedAt,
				updatedDay,
				modifiedBy,
				seo_title,
				seo_description,
			});

		if (signalQuickUpdate.error) return res.status(STATUS_CODE.SERVER_ERROR).json(signalQuickUpdate);

		return res.status(STATUS_CODE.OK).json(signalQuickUpdate);
	}

	remove = async (req, res, next) => {
		const { id } = req.params;

		const signalGetResult = await this.service.get({ _id: id });
		if (signalGetResult.error) res.status(STATUS_CODE.BAD_REQUEST).json(signalGetResult);

		const result = signalGetResult.data;

		const removeSignals = await Promise.all([
			this.service.remove(result._id),
		]);

		const data = removeSignals.data
		return res.status(STATUS_CODE.OK).json(RESPONSE_DATA({ data, message: MESSAGES.DELETE_SUCCEED }));
	};
}

module.exports = new FaqCategoryController();
