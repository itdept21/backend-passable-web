const { roleChecker } = require("../helpers");

class GuardController {
	static async needLogin(req, res, next) {
		if (req.session && req.session.passport && req.session.passport.user) {
			let task = req.body.task ? req.body.task : req.query.task ? req.query.task : null;
			const { id: userTargetID } = req.params;
			const permission = await roleChecker.checkRole(
				req.session.passport.user.role,
				task,
				roleChecker.roleValue[task],
				userTargetID
			);
			req.user = req.session.passport.user;
			if (!task || permission) {
				return next();
			} else {
				return res.json({ error: true, message: "Bạn không có quyền làm việc này !", data: null });
			}
		}
		return res.redirect("/admin");
	}

	static needNotLogin(req, res, next) {
		if (req.session.passport && req.session.passport) res.redirect("/admin/dashboard");
		else {
			next();
		}
	}
}

module.exports = GuardController;
