const sendMail = require("../../helpers/nodeMailer");
const { process_email, host_admin } = require("../../constants/mail.constants");
const { generateEmailOthersContent } = require("./templateSendMail");

async function SendSystem(data, attachments, subjectEmail) {
    const content = generateEmailOthersContent(data);
    const listEmail = [process_email]
    const _dataSendMail = {
        ...content,
        attachments: attachments
    };
    sendMail(listEmail, subjectEmail, _dataSendMail);

};

async function SendCustomer(data, attachments, subjectEmail) {
    const content = generateEmailOthersContent(data);
    const _dataSendMail = {
        ...content,
        attachments: attachments
    };
    sendMail(data.email, subjectEmail, _dataSendMail);

};

class PassableEssayEmail {
    #_contactOrder;
    constructor({
        contactOrder
    }) {
        this.#_contactOrder = contactOrder;
    }

    PassableEssaySend() {
        const subjectEmail = this.#_contactOrder.subjectEmail
        const attachments = this.#_contactOrder?.files?.map((e) => {
            return {
                filename: e.name,
                path: host_admin + e.src,
            }
        }) || []
        delete this.#_contactOrder.subjectEmail

        SendSystem(this.#_contactOrder, attachments, subjectEmail)
        if (this.#_contactOrder.email && this.#_contactOrder.email != "")
            SendCustomer(this.#_contactOrder, attachments, subjectEmail)
        console.log("PassableEssaySend");
    }

}

module.exports = {
    PassableEssayEmail
}
