var express = require("express");
var router = express.Router();
const passport = require("passport");

const {
    AdminController,
    GuardController,
    ImageController,
    UserController,
    DocController,
    FileController,
    LayoutController,
} = require("../controllers");
const PageComponentController = require("../controllers/pageComponent.controller");

const { session, passportConfig, fileUploader, parseFormData } = require("../helpers");

passportConfig.initialize();

session.register(router);

// ---------------------- GET - RENDER REQUEST --------------------
router.get("/", GuardController.needNotLogin, AdminController.renderLoginPage);

router.get("/logout", GuardController.needLogin, AdminController.destroySession);

router.get("/dashboard", GuardController.needLogin, AdminController.renderAdminDashboard);

router.get("/gallery", GuardController.needLogin, ImageController.renderGallery);

router.get("/documents-library", /* GuardController.needLogin, */ DocController.renderGallery);

router.get("/upload-ckeditor", GuardController.needLogin, ImageController.renderGalleryCkeditor);

// --------------------- GET REQUEST -----------------------------
router.get("/images", GuardController.needLogin, ImageController.getList);

router.get("/users", GuardController.needLogin, UserController.renderList);

router.get("/user/password/:userID?", GuardController.needLogin, UserController.renderUpdatePassword);


// --------------------- GET - RENDER REQUEST LAYOUT -----------------------------
router.get("/page-home", GuardController.needLogin, LayoutController.renderEditHomePage);

router.get("/page-about", GuardController.needLogin, LayoutController.renderEditAboutPage);

router.get("/seo/:pageName", GuardController.needLogin, LayoutController.renderSeoPage);

// -------------------- POST REQUEST -----------------------------
router.post(
    "/",
    AdminController.refillPassword,
    passport.authenticate("local.login", passportConfig.PASSPORT_LOGIN_OPTIONS)
);

router.post(
    "/images",
    GuardController.needLogin,
    fileUploader.uploadImage.fields([{ maxCount: 1, name: "images" }]),
    fileUploader.saveSingleImagesToDB,
    ImageController.create
);

router.post(
    "/documents",
    GuardController.needLogin,
    fileUploader.uploadDocument.fields([{ maxCount: 10, name: "docs" }]),
    fileUploader.saveSingleDocsToDB,
    DocController.create
);

router.post(
    "/files",
    // GuardController.needLogin,
    fileUploader.uploadFile.fields([{ maxCount: 20, name: "files" }]),
    fileUploader.saveSingleFilesToDB,
    FileController.create
);

router.put(
    "/images",
    GuardController.needLogin,
    parseFormData,
    ImageController.update
);

router.put(
    "/robot",
    GuardController.needLogin,
    fileUploader.uploadRobotTxt.single("file"),
    AdminController.updatePublicFile
);

router.post("/user", GuardController.needLogin, fileUploader.uploadImage.none(), UserController.create);

// -------------------- PUT REQUEST ------------------------------
router.put(
    "/page-component",
    GuardController.needLogin,
    fileUploader.uploadImage.none(),
    PageComponentController.createOrUpdate
);

// -------------------- DELETE REQUEST ----------------------------
router.delete("/images/:id", GuardController.needLogin, ImageController.remove);
router.delete("/user/:id", GuardController.needLogin, UserController.remove);
router.delete("/docs/:id", GuardController.needLogin, DocController.remove);

// -------------------- PATCH REQUEST ----------------------------
router.patch(
    "/user/password",
    GuardController.needLogin,
    fileUploader.uploadImage.none(),
    UserController.updatePassword
);

router.use(require('./admin.policyCategory'))
router.use(require('./admin.policy'))
router.use(require('./admin.faqCategory'))
router.use(require('./admin.faq'))
router.use(require('./admin.servicesCategory'))
router.use(require('./admin.services'))
router.use(require('./admin.promotion'))
router.use(require('./admin.paymentMethod'))

module.exports = router;
