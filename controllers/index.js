const AdminController = require("./admin.controller");
const GuardController = require("./guard.controller");
const ImageController = require("./image.controller");
const UserController = require("./user.controller");
const IndexController = require("./index.controller");
const DocController = require("./doc.controller");
const FileController = require("./file.controller");
const LayoutController = require("./layout.controller");

module.exports = {
	AdminController,
	GuardController,
	ImageController,
	UserController,
	IndexController,
	DocController,
	FileController,
	LayoutController,
};
