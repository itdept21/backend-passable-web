const mongoose = require('mongoose');
const excelToJson = require('convert-excel-to-json');

mongoose.Promise = global.Promise;

const readFileFromExcel = async (file) => {
    const dataEx = excelToJson({
        sourceFile: file,
        header: {
            rows: 1 // 2, 3, 4, etc.
        },
        columnToKey: {
            '*': '{{columnHeader}}'
        }
    });
    const data = dataEx.Sheet1;
    console.log("data", data);

    const result = [];
    for (let i = 0; i < data.length; i++) {
        const obj = {
            oldUrl: data[i].oldUrl,
            url: data[i]?.url || "",
            statusCode: data[i]?.statusCode || 301,
        }
        result.push(obj);
    }
    return result;
}

module.exports = {
    readFileFromExcel,
}