var express = require("express");
var router = express.Router();
const { IndexController } = require("../controllers");
const { fileUploader, parseFormData } = require("../helpers");
const { FileController, } = require("../controllers");
/* GET home page. */

router.use((req, res, next) => {
	console.log(req?.originalUrl);

	if (req?.originalUrl.match(/\/admin|\.php|\.html/)) {
		return res.status(404).json({ error: true, message: 'notfound', data: null })
	}
	return next();
})


// ==================================== API NEW =======================================

// home
router.get("/", IndexController.getDataHomeEn);

// ==================================== API policy =======================================
router.get("/promotions", IndexController.getPromotion);

// ==================================== API policy =======================================
router.get("/payment-methods", IndexController.getPaymentMethod);

// ==================================== API policy =======================================
router.get("/policy", IndexController.getDataPolicyEn);
router.get("/:slugPolicy", IndexController.getPolicyDetailEn);

router.post("/contact-order",
	parseFormData,
	IndexController.contactOrder
);

router.post("/upload-files",
	fileUploader.uploadFile.fields([{ maxCount: 20, name: "files" }]),
	fileUploader.saveSingleFilesToDB,
	FileController.create
);


module.exports = router;
