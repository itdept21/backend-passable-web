// @ts-nocheck
const BaseCrudController = require("./baseCrud.controller");
const service = require("../services/contactOrder.services");
const FileService = require("../services/file.services");
const policyCategoryService = require('../services/policyCategory.services');
const policyService = require('../services/policy.services');
const promotionService = require('../services/promotion.services');
const paymentMethodService = require('../services/paymentMethod.services');
const { PassableEssayEmail } = require("./sendEmail/passableEssayEmail");
const dayjs = require('dayjs');
var utc = require('dayjs/plugin/utc');
var timezone = require('dayjs/plugin/timezone');
const { AddRowSheetGoogle } = require("./addRowGoogleSheet/addRowSheet");
const { host_admin } = require("../constants/mail.constants");
const { formatDate } = require("../helpers/dateTimeFormat");
dayjs.extend(utc);
dayjs.extend(timezone);
const servicePageComponent = require('../services/pageComponent.services');
const serviceServicesCategory = require('../services/servicesCategory.services');
const { MESSAGES, STATUS_CODE, RESPONSE_DATA } = require("../constants/base.constants");
const { POPULATE_HOME } = require("../constants/populate.constants");
const { STRUCTURE_DATA_TYPE } = require("../constants/structureData.constants");

class IndexController extends BaseCrudController {
	constructor() {
		super(service);
	}

	contactOrder = async (req, res, next) => {

		try {
			let {
				data,
				files = [],
			} = req.body;
			let {
				name = 'Name',
				email = 'Email',
				phoneNumber = 'Phone Number',
				dueDate,
				subject = 'Subject',
				wordCount = 0,
				major = 'Major',
				institution = 'Institution',
			} = data
			// const _files = files?.[0]?.files?.map((e) => e._id) || null
			const _files = files?.map((e) => {
				return e._id
			}) || null
			// create contact order model
			const signalCreateContactOrder = await service.create({
				name,
				email,
				phoneNumber,
				dueDate,
				subject,
				wordCount: wordCount,
				major,
				institution,
				files: _files,
			});

			if (signalCreateContactOrder.error) return res.status(500).json(signalCreateContactOrder);
			const resultOrder = signalCreateContactOrder.data || null;

			const signalResponse = await service.get(
				{ _id: resultOrder._id, },
				[
					{
						path: 'files',
					},
				]
			);
			const _contactOrder = signalResponse.data;

			// update status file model
			if (_contactOrder?.files?.length > 0) {
				const _ids = _contactOrder?.files.map((e) => e._id);
				await FileService.updateMany(
					{ _id: { $in: _ids } },
					{ status: 2 },
				);
			}

			const _dueDate = formatDate(
				new Date(
					dayjs(dueDate)
						.tz('asia/ho_chi_minh')
						.format('YYYY-MM-DDTHH:mm'),
				),
			);

			// send mail
			if (_contactOrder != null) {
				const _dataContactOrder = {
					name: _contactOrder.name,
					email: _contactOrder.email,
					phoneNumber: _contactOrder.phoneNumber,
					dueDate: _dueDate,
					subject: _contactOrder.subject,
					wordCount: _contactOrder.wordCount,
					major: _contactOrder.major,
					institution: _contactOrder.institution,
					files: _contactOrder?.files || null,
					subjectEmail: ' Submission Order ' +
						dayjs(new Date()).tz('asia/ho_chi_minh').format('YYYY-MM-DD') +
						' '
				}
				const passableEssayEmail = new PassableEssayEmail(
					{
						contactOrder: _dataContactOrder,
					}
				);
				await passableEssayEmail.PassableEssaySend();
			}

			// add row spreadsheets
			if (_contactOrder != null) {
				// ghép chuổi src trong _contactOrder?.files
				let src = ''
				if (_contactOrder?.files?.length > 0) {
					_contactOrder?.files?.forEach((e) => {
						src += host_admin + e.src + '\n'
					})
				}

				const _dataContactOrderAddRow = {
					name: _contactOrder.name,
					email: _contactOrder.email,
					phoneNumber: _contactOrder.phoneNumber,
					dueDate: _dueDate,
					subject: _contactOrder.subject,
					wordCount: _contactOrder.wordCount,
					major: _contactOrder.major,
					institution: _contactOrder.institution,
					file: src,
				}

				const addRowSheetGoogle = new AddRowSheetGoogle(
					{
						contactOrder: _dataContactOrderAddRow,
					}
				);
				await addRowSheetGoogle.PassableEssayAddRowSheetGoogle();
			}
			return res.status(201).json(signalResponse);
		} catch (e) {
			return res.status(201).json({
				error: true,
				data: null,
				message: e.message
			});
		}
	}

	getDataHomeEn = async (req, res, next) => {
		const lang = 'en'

		///////////// Above the fold 
		const signalGetFold1 = await servicePageComponent.get(
			{
				page: { $in: ["home"] },
				name: { $in: ["fold1"] },
				lang,
				parent: null,
			},
			POPULATE_HOME,
		);
		const fold1 = {
			_id: signalGetFold1.data?._id || "",
			lang: signalGetFold1.data?.lang || "en",
			title: signalGetFold1.data?.title || "",
			content: signalGetFold1.data?.content || "",
			buttonName: signalGetFold1.data?.buttonName || "",
			buttonLink: signalGetFold1.data?.buttonLink || "",
			profile: {
				position: signalGetFold1.data?.opt1?.description || "",
				name: signalGetFold1.data?.opt1?.name || "",
				talk: signalGetFold1.data?.opt1?.content || "",
				image: signalGetFold1.data?.images?.[0] || {},
			}
		};

		///////////// Section 1: Stats
		const signalGetSection1 = await servicePageComponent.get(
			{
				page: { $in: ["home"] },
				name: { $in: ["section1"] },
				lang,
				parent: null,
			},
			POPULATE_HOME,
		);

		const section1 = {
			_id: signalGetSection1.data?._id || "",
			lang: signalGetSection1.data?.lang || "en",
			title: signalGetSection1.data?.title || "",
			content: signalGetSection1.data?.content || "",
			child: signalGetSection1.data?.child?.map((e) => {
				return {
					number: e?.title || "",
					description: e?.content || "",
				}
			})
		};

		///////////// Section 2: Greeting video
		const signalGetSection2 = await servicePageComponent.get(
			{
				page: { $in: ["home"] },
				name: { $in: ["section2"] },
				lang,
				parent: null,
			},
			POPULATE_HOME,
		);

		const section2 = {
			_id: signalGetSection2.data?._id || "",
			lang: signalGetSection2.data?.lang || "en",
			title: signalGetSection2.data?.title || "",
			content: signalGetSection2.data?.content || "",
			url: signalGetSection2.data?.url || "",
			socialMedia: signalGetSection2.data?.child?.map((e) => {
				return {
					// ...e,
					image: e?.images?.[0] || {},
					url: e?.url || "",
				}
			})
		};

		///////////// Section 3: Greeting video
		const signalGetSection3 = await servicePageComponent.get(
			{
				page: { $in: ["home"] },
				name: { $in: ["section3"] },
				lang,
				parent: null,
			},
			POPULATE_HOME,
		);

		const section3 = {
			_id: signalGetSection3.data?._id || "",
			lang: signalGetSection3.data?.lang || "en",
			header1: signalGetSection3.data?.title || "",
			content: signalGetSection3.data?.content || "",
			header2: signalGetSection3.data?.description || "",
			expert: signalGetSection3.data?.child?.map((e) => {
				return {
					// ...e,
					image: e?.images?.[0] || {},
					universityName: e?.content || "",
					degree: e?.title || "",
					description: e?.description || "",
				}
			})
		};

		///////////// Section 4: Services
		const signalGetSection4 = await servicePageComponent.get(
			{
				page: { $in: ["home"] },
				name: { $in: ["section4"] },
				lang,
				parent: null,
			},
			POPULATE_HOME,
		);

		const _serviceCateId = signalGetSection4?.data?.serviceCategory?.map(e => e._id) || [];
		const signalGetCateChild = await serviceServicesCategory.getList({
			matchConditions: {
				parent: { $in: _serviceCateId },
			}
		});
		const cateChild = signalGetCateChild.data || []

		const section4 = {
			_id: signalGetSection4.data?._id || "",
			lang: signalGetSection4.data?.lang || "en",
			title: signalGetSection4.data?.title || "",
			stats: signalGetSection4.data?.child?.map((e) => {
				return {
					title: e?.title || "",
					content: e?.content || "",
				}
			}),
			services: signalGetSection4.data?.serviceCategory?.map((e) => {
				const _child = cateChild.filter((_) => _.parent.toString() == e._id.toString());
				return {
					// ...e,
					name: e?.name || "",
					slug: e?.slug || "",
					typeOfServices: e?.typeOfServices || "",
					child: _child.map((_) => {
						return {
							name: _.name,
							description: _.description,
							content: _.content,
							slug: _.slug,
							typeOfServices: _.typeOfServices,
						}
					})
				}
			}),
		};

		///////////// Section 5: Services
		const signalGetSection5 = await servicePageComponent.get(
			{
				page: { $in: ["home"] },
				name: { $in: ["section5"] },
				lang,
				parent: null,
			},
			POPULATE_HOME,
		);

		const section5 = {
			_id: signalGetSection5.data?._id || "",
			lang: signalGetSection5.data?.lang || "en",
			title: signalGetSection5.data?.title || "",
			content: signalGetSection5.data?.content || "",
			story: signalGetSection5.data?.child?.map((e) => {
				return {
					// ...e,
					logo: e?.images?.[0] || {},
					storyTitle: e?.title || "",
					universityName: e?.description || "",
					content: e?.content || "",
				}
			})
		};

		///////////// Section 6: Who’s this for
		const signalGetSection6 = await servicePageComponent.get(
			{
				page: { $in: ["home"] },
				name: { $in: ["section6"] },
				lang,
				parent: null,
			},
			POPULATE_HOME,
		);

		const section6 = {
			_id: signalGetSection6.data?._id || "",
			lang: signalGetSection6.data?.lang || "en",
			title: signalGetSection6.data?.title || "",
			audience: signalGetSection6.data?.child?.map((e) => {
				return {
					target: e?.title || "",
					description: e?.content || "",
				}
			})
		};

		///////////// Section 7: About Passable Essay Service
		const signalGetSection7 = await servicePageComponent.get(
			{
				page: { $in: ["home"] },
				name: { $in: ["section7"] },
				lang,
				parent: null,
			},
			POPULATE_HOME,
		);

		const section7 = {
			_id: signalGetSection7.data?._id || "",
			lang: signalGetSection7.data?.lang || "en",
			images: signalGetSection7.data?.images?.[0] || {},
			title: signalGetSection7.data?.title || "",
			content: signalGetSection7.data?.content || "",
			socialMedia: signalGetSection7.data?.child?.map((e) => {
				return {
					image: e?.images?.[0] || {},
					url: e?.url || "",
				}
			})
		};

		///////////// Section 8: Feedback 
		const signalGetSection8 = await servicePageComponent.get(
			{
				page: { $in: ["home"] },
				name: { $in: ["section8"] },
				lang,
				parent: null,
			},
			POPULATE_HOME,
		);

		const section8 = {
			_id: signalGetSection8.data?._id || "",
			lang: signalGetSection8.data?.lang || "en",
			title: signalGetSection8.data?.title || "",
			content: signalGetSection8.data?.content || "",
			comment: signalGetSection8.data?.child?.map((e) => {
				return {
					image: e?.images?.[0] || {},
					customerName: e?.title || "",
					commentDate: e?.description || "",
					commentDetail: e?.content || "",
				}
			})
		};

		///////////// Section 9: Price change
		const signalGetSection9 = await servicePageComponent.get(
			{
				page: { $in: ["home"] },
				name: { $in: ["section9"] },
				lang,
				parent: null,
			},
			POPULATE_HOME,
		);

		const section9 = {
			_id: signalGetSection9.data?._id || "",
			lang: signalGetSection9.data?.lang || "en",
			title: signalGetSection9.data?.title || "",
			limitedOffer: {
				title: signalGetSection9.data?.opt1?.title || "",
				description: signalGetSection9.data?.opt1?.content || "",
			},
			buttonName: signalGetSection9.data?.buttonName || "",
			buttonLink: signalGetSection9.data?.buttonLink || "",

			price: signalGetSection9.data?.child?.map((e) => {
				return {
					name: e?.title || "",
					detail: e?.content || "",
				}
			})
		};

		///////////// Section 10: Package list
		const signalGetSection10 = await servicePageComponent.get(
			{
				page: { $in: ["home"] },
				name: { $in: ["section10"] },
				lang,
				parent: null,
			},
			POPULATE_HOME,
		);

		const section10 = {
			_id: signalGetSection10.data?._id || "",
			lang: signalGetSection10.data?.lang || "en",
			title: signalGetSection10.data?.title || "",
			package: signalGetSection10.data?.packages?.map((e) => {
				return {
					// ...e,
					name: e?.name || "",
					slug: e?.slug || "",
					description: e?.description || "",
					content: e?.content || "",
					wordCount: e?.wordCount || "",
					typeOfServices: e?.typeOfServices || "",
					titleRate: e?.titleRate || "",
					newRate: e?.newRate || "",
					oldRate: e?.oldRate || "",
					recommend: e?.recommend || "",
					buttonName: e?.buttonName || "",
				}
			}),
		};

		///////////// Section 10: Package list
		const signalGetFaqs = await servicePageComponent.get(
			{
				page: { $in: ["home"] },
				name: { $in: ["faqs"] },
				lang,
				parent: null,
			},
			POPULATE_HOME,
		);

		const faqs = {
			_id: signalGetFaqs.data?._id || "",
			lang: signalGetFaqs.data?.lang || "en",
			title: signalGetFaqs.data?.title || "",
			content: signalGetFaqs.data?.content || "",
			child: signalGetFaqs.data?.faqs?.map((e) => {
				return {
					// ...e,
					name: e?.name || "",
					description: e?.description || "",
					content: e?.content || "",
				}
			}),
		};

		///////////// SEO Home
		const signalGetSEO = await servicePageComponent.get(
			{
				page: { $in: ["home"] },
				name: { $in: ["seo"] },
				lang,
				parent: null,
			},
			['images']
		);
		console.log("signalGetSEO", signalGetSEO)
		const seo = {
			seo_title: signalGetSEO?.data?.title || '',
			seo_description: signalGetSEO?.data?.description || '',
			seo_h1: signalGetSEO?.data?.seo_h1 || '',
			seo_keyword: signalGetSEO?.data?.content || '',
			seo_schema: signalGetSEO?.data?.seo_schema || '',
			seo_canonical: signalGetSEO?.data?.seo_canonical || '',
			seo_lang: signalGetSEO?.data?.seo_lang || signalGetSEO?.data?.lang || 'en',
			seo_image: signalGetSEO?.data?.images[0]?.src ? {
				src: signalGetSEO?.data?.images[0]?.src || '',
				name: signalGetSEO?.data?.images[0]?.name || '',
				alt: signalGetSEO?.data?.images[0]?.alt || '',
			} : null,
		}

		const dataResult = {
			error: false,
			data: {
				originalUrl: req?.originalUrl,
				fold1,
				section1,
				section2,
				section3,
				section4,
				section5,
				section6,
				section7,
				section8,
				section9,
				section10,
				faqs,
				seo,
				typePage: 'home',
				page: STRUCTURE_DATA_TYPE.HOME,
			},
			message: MESSAGES.GET_SUCCEED
		}

		// CacheRoute.setExV2(req, dataResult, { ex: time_cache })

		return res.status(STATUS_CODE.OK).json(RESPONSE_DATA(dataResult));
	};

	getPromotion = async (req, res, next) => {
		const signalGetPolicyCategory = await promotionService.getList(
			{
				matchConditions: {
					status: { $gte: 1 },
					dateExpired: { $gte: new Date() },
				},
				populates: [
					{
						path: 'icon',
						select: '_id name src alt title caption'
					},
				],
				select: "lang price minRateOrder type status code name description icon"
			},
		)

		const dataPromotion = signalGetPolicyCategory?.data;
		if (!dataPromotion) return next();

		const dataResult = {
			error: false,
			data: {
				originalUrl: req?.originalUrl,
				promotion: dataPromotion,
				typePage: 'promotion',
			},
			message: MESSAGES.GET_SUCCEED
		}

		return res.status(STATUS_CODE.OK).json(RESPONSE_DATA(dataResult));
	};

	getPaymentMethod = async (req, res, next) => {
		const signalGetPaymentMethod = await paymentMethodService.getList(
			{
				matchConditions: {
					status: { $gte: 1 },
				},
				populates: [
					{
						path: 'thumbnail',
						select: '_id name src alt title caption'
					}, {
						path: 'qrCode',
						select: '_id name src alt title caption'
					},
				],
				select: "bankName accountNumber accountName swiftCode postCode email  "
			},
		)

		const dataPaymentMethod = signalGetPaymentMethod?.data;
		if (!dataPaymentMethod) return next();

		const dataResult = {
			error: false,
			data: {
				originalUrl: req?.originalUrl,
				paymentMethod: dataPaymentMethod,
				typePage: 'payment_methods',
			},
			message: MESSAGES.GET_SUCCEED
		}

		return res.status(STATUS_CODE.OK).json(RESPONSE_DATA(dataResult));
	};

	getDataPolicyEn = async (req, res, next) => {
		const signalGetPolicyCategory = await policyCategoryService.getList(
			{
				matchConditions: {
					status: { $gte: 1 },
					lang: 'en'
				},
				populates: [
					{
						path: 'thumbnail',
						select: '_id name src alt title caption'
					},
				],
				select: "_id name description content lang publishedAt slug status"
			},

		)

		const dataPolicy = signalGetPolicyCategory?.data;
		if (!dataPolicy) return next();

		const dataResult = {
			error: false,
			data: {
				originalUrl: req?.originalUrl,
				policy: dataPolicy,
				typePage: 'policy',
				page: STRUCTURE_DATA_TYPE.POLICY,
			},
			message: MESSAGES.GET_SUCCEED
		}

		return res.status(STATUS_CODE.OK).json(RESPONSE_DATA(dataResult));
	};

	getPolicyDetailEn = async (req, res, next) => {
		const { slugPolicy } = req.params;
		const signalGetPolicyCategory = await policyCategoryService.get(
			{
				slug: slugPolicy,
				status: { $gte: 1 },
				lang: 'en'
			},
			[
				{
					path: 'thumbnail',
					select: '_id name src alt title caption'
				},
				{
					path: 'seo_image',
					select: '_id name src alt title caption'
				},
			],
			"_id name description content lang publishedAt slug status seo_canonical seo_description seo_keywords seo_lang seo_schema seo_title "
		);
		const dataDetail = signalGetPolicyCategory?.data;
		if (!dataDetail) return next();

		const signalGetPolicy = await policyService.getList({
			matchConditions: {
				category: dataDetail._id,
				status: { $gte: 1 },
				lang: 'en'
			},
			populates: [
				{
					path: 'category',
					select: '_id name slug'
				},
			],
			sort: { displayOrder: 1, modifiedAt: -1, createdAt: -1 },
			select: "_id lang category name toc menuList content displayOrder status createdAt modifiedAt"

		});
		const dataContent = signalGetPolicy?.data || [];

		const detail = {
			_id: dataDetail?._id || "",
			name: dataDetail?.name || "",
			description: dataDetail?.description || "",
			content: dataDetail?.content || "",
			lang: dataDetail?.lang || "",
			publishedAt: dataDetail?.publishedAt || "",
			slug: dataDetail?.slug || "",
			status: dataDetail?.status || "",
			thumbnail: dataDetail?.thumbnail || {},
			info: dataContent || [],
		}

		const seo = {
			seo_canonical: dataDetail?.seo_canonical || "",
			seo_description: dataDetail?.seo_description || "",
			seo_keywords: dataDetail?.seo_keywords || "",
			seo_lang: dataDetail?.seo_lang || "",
			seo_schema: dataDetail?.seo_schema || "",
			seo_title: dataDetail?.seo_title || "",
			seo_image: dataDetail?.seo_image || {},
		};

		// create breadcrumb on page policy detail
		const breadcrumbPolicy = [
			{
				id: 1,
				name: "HomePage",
				url: `/`,
			},
			{
				id: 2,
				name: dataDetail?.name || "",
				url: `/${dataDetail?.slug || ""}`,
			},
		]

		const dataResult = {
			error: false,
			data: {
				originalUrl: slugPolicy,
				breadcrumb: breadcrumbPolicy,
				detail: detail,
				seo: seo,
				typePage: 'detail_policy',
				page: STRUCTURE_DATA_TYPE.POLICY,
			},
			message: MESSAGES.GET_SUCCEED
		}

		return res.status(STATUS_CODE.OK).json(RESPONSE_DATA(dataResult));
	}

}

module.exports = new IndexController();
