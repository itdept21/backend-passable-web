var express = require("express");
var router = express.Router();

const controller = require("../controllers/faqCategory.controller.js");
const { parseFormData } = require("../helpers/index.js");
const { GuardController } = require("../controllers/index.js");
const viewController = require("../controllers/view/faqCategory.controller.js");

// // -------------------- GET DATA - RENDER VIEW ----------------------------
router.get("/faq-categories/:id", GuardController.needLogin, viewController.renderEdit);
router.get("/faq-categories", GuardController.needLogin, viewController.renderList);

// // -------------------- CREATE OR UPDATE ----------------------------
router.post("/faq-categories", GuardController.needLogin, parseFormData, controller.createID);
router.post("/faq-categories/quick-update/:id", GuardController.needLogin, parseFormData, controller.quickUpdate);
router.put('/faq-categories/:id', GuardController.needLogin, parseFormData, controller.createOrUpdate);
router.put('/faq-categories/:id/status/:status', GuardController.needLogin, parseFormData, controller.updateStatus);

// // -------------------- DELETE ----------------------------
router.delete("/faq-categories/:id", GuardController.needLogin, controller.remove);

module.exports = router;

