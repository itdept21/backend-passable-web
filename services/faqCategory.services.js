// @ts-nocheck
const BaseService = require('./base.services');
const model = require('../models/faqCategory.model');

class FaqCategoryService extends BaseService {
    constructor() {
        super(model);
    };

    // count child by parent
    countChildByParent = async (parent) => {
        const signalGet = await this.count({ parent });
        return signalGet.data;
    };

}

module.exports = new FaqCategoryService();