const mongoose = require('mongoose');
const ADMIN_COLL = require('../models/admin.model');
const { mongoDbUri } = require('../config/cf_mongodb');

mongoose.Promise = global.Promise;

// Connect MongoDB at default port 27017.
mongoose.connect(mongoDbUri, {
    useNewUrlParser: true,
    useCreateIndex: true,
}, async (err) => {
    if (!err) {
        console.log('MongoDB Connection Succeeded.')
        const usernameArr = ['superadmin']
        const nameArr = ['ADMINISTRATOR']

        for (let i = 0; i < usernameArr.length; i++) {
            let user = new ADMIN_COLL({ username: usernameArr[i], role: 0, password: '123456', fullname: nameArr[i], email: '', createdAt: new Date, modifiedAt: new Date })
            user.setPassword(user.password)
            const doc = await user.save()
            console.log(doc)
        }

        await mongoose.connection.close()
    } else {
        console.log('Error in DB connection: ' + err)
    }
});


