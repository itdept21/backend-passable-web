const BaseService = require('./base.services');
const model = require('../models/servicesCategory.model');

class ServicesCategoryService extends BaseService {
    constructor() {
        super(model);
    }
}

module.exports = new ServicesCategoryService();