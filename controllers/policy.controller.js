// @ts-nocheck
const BaseCrudController = require("./baseCrud.controller");
const service = require("../services/policy.services");
const { STATUS_CODE, MESSAGES, RESPONSE_DATA, } = require("../constants/base.constants");
const { createSlugFromTitle } = require("../helpers/common");
const createToc = require("../helpers/createToc");
const { DATE_FORMAT_FN } = require("../helpers/dateTimeFormat");

class PolicyController extends BaseCrudController {
	constructor() {
		super(service);
	}

	createEmptyPolicy = async (req, res, next) => {
		const signalResponse = await this.service.createID();
		res.status(signalResponse.error ? STATUS_CODE.BAD_REQUEST : STATUS_CODE.CREATED).json(signalResponse);
	};

	createOrUpdate = async (req, res, next) => {
		try {
			let id = req.params.id.trim();
			const dataUpdate = req.body;

			dataUpdate['category'] = dataUpdate['category'] ? dataUpdate['category'] : null;
			dataUpdate['thumbnail'] = dataUpdate['thumbnail'] ? dataUpdate['thumbnail'] : null;
			dataUpdate['slug'] = dataUpdate['slug'] ? dataUpdate['slug'] : createSlugFromTitle(dataUpdate['name']) + "-" + DATE_FORMAT_FN(new Date(), 'YYMMDDHH');

			dataUpdate['seo_image'] = dataUpdate['seo_image'] ? dataUpdate['seo_image'] : null;

			if (!dataUpdate?.createdAt) dataUpdate.createdAt = new Date();
			if (!dataUpdate?.createdBy) dataUpdate.createdBy = req.user.username;
			dataUpdate.modifiedBy = req.user.username;

			// create TOC
			const dataCreate = createToc(dataUpdate.content);
			dataUpdate.toc = dataCreate.html;
			dataUpdate.content = dataCreate.content;
			dataUpdate.menuList = dataCreate.menuList.map((e) => {
				return {
					...e,
					ref: id,
				};
			});

			dataUpdate.menuList = dataUpdate.menuList.filter((e) => e.level < 3);

			const signalUpdatePolicy = await this.service.createOrUpdate({ _id: id }, dataUpdate);
			if (signalUpdatePolicy.error) return res.status(STATUS_CODE.SERVER_ERROR).json(signalUpdatePolicy);
			const policy = signalUpdatePolicy.data;

			const responseJson = RESPONSE_DATA({ data: policy, message: MESSAGES.CREATED_SUCCEED });
			return res.status(STATUS_CODE.OK).json(responseJson);
		} catch (error) {
			return res.status(400).json(RESPONSE_DATA({ error: true, message: error.message }));
		}
	};

	updatePolicyStatus = async (req, res, next) => {
		let { id, status } = req.params;
		status = parseInt(status);
		const modifiedBy = req.user.username;

		if ([0, 1, 2].indexOf(parseInt(status)) < 0)
			return res.status(STATUS_CODE.BAD_REQUEST).json(RESPONSE_DATA({ error: true, message: "invalid_status" }));

		const signalUpdate = await this.service.update({ _id: id }, { status, modifiedBy });

		if (signalUpdate.error) return res.status(STATUS_CODE.SERVER_ERROR).json(signalUpdate);

		return res.status(STATUS_CODE.OK).json(signalUpdate);
	};

	quickUpdate = async (req, res, next) => {
		const { id } = req.params;
		const { name, displayOrder, seo_title, seo_description } = req.body;
		const modifiedBy = req.user.username;

		const signalGetPolicy = await this.service.get({ _id: id });

		const policy = signalGetPolicy.data;

		if (!policy) return res.status(400).json(signalGetPolicy)
		// quick update
		const signalQuickUpdate = await this.service.update(
			{
				_id: id
			},
			{
				name,
				displayOrder,
				modifiedBy,
				seo_title,
				seo_description,
			});

		if (signalQuickUpdate.error) return res.status(STATUS_CODE.SERVER_ERROR).json(signalQuickUpdate);

		return res.status(STATUS_CODE.OK).json(signalQuickUpdate);
	}

	remove = async (req, res, next) => {
		const { id } = req.params;

		const signalGetPolicy = await this.service.get({ _id: id });
		if (signalGetPolicy.error) res.status(STATUS_CODE.BAD_REQUEST).json(signalGetPolicy);

		const policy = signalGetPolicy.data;

		const removeSignals = await this.service.remove(policy._id);

		const data = removeSignals.data;
		return res.status(STATUS_CODE.OK).json(RESPONSE_DATA({ data, message: MESSAGES.DELETE_SUCCEED }));
	};
}

module.exports = new PolicyController();
