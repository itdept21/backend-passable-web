// @ts-nocheck
const ADMIN_MODEL = require("../models/admin.model");

exports.roleValue = {
	// 0 : admin
	// 1 : seo
	// 2 : editor

	CREATE_ARTICLE: 2,
	UPDATE_ARTICLE: 2,
	PUBLISH_ARTICLE: 3,
	UPDATE_CATEGORY: 1,
	UPDATE_REDIRECT: 1,

	DELETE_CATEGORY_ARTICLE: 1,

	KICK_EDITOR: 0,
	KICK_SEO: 0,
	KICK_ADMIN: 0,

	CREATE_EDITOR: 0,
	CREATE_SEO: 0,
};

exports.roles = Object.keys(this.roleValue);

/**
 * Check permission of admin members before process admin task
 * @param {number} role        admin role number
 * @param {string} task        task name
 * @param {number} roleTask    task role number
 * @param {ObjectId} adminID   admin ObjectID
 * @returns {boolean}
 */
exports.checkRole = async (role, task, roleTask, adminID) => {
	// const { role } = userInfo
	if (task == "KICK_SEO" || task == "KICK_EDITOR") {
		const targetUser = await ADMIN_MODEL.findById(targetUserID).select("role username");
		const { role: targetRole } = targetUser;
		return role > targetRole;
	} else {
		return role <= roleTask ? true : false;
	}
};
