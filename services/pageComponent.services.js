// @ts-nocheck
const BaseService = require('./base.services');
const model = require('../models/pageComponent.model');
// const { formatData, formatDataSEO } = require('../constants/base.constants');

class PageComponentService extends BaseService {
    constructor() {
        super(model);
    }

    // getDataByPageName = async (page, name, lang, populate) => {
    //     const signalGetFold1 = await this.get(
    //         {
    //             page: { $in: [page] },
    //             name: { $in: [name] },
    //             lang,
    //             parent: null,
    //         },
    //         populate,
    //     );
    //     const data = formatData(signalGetFold1.data)
    //     return data
    // }

    // getDataSEOByPageName = async (page, name, lang, populate) => {
    //     const signalGetFold1 = await this.get(
    //         {
    //             page: { $in: [page] },
    //             name: { $in: [name] },
    //             lang,
    //             parent: null,
    //         },
    //         populate,
    //     );
    //     const data = formatDataSEO(signalGetFold1.data)
    //     return data
    // }

    // getDataFaqCateByPageName = async (page, name, lang, populate) => {
    //     const signalGetFold1 = await this.get(
    //         {
    //             page: { $in: [page] },
    //             name: { $in: [name] },
    //             lang,
    //             parent: null,
    //         },
    //         populate,
    //     );
    //     return {
    //         _id: signalGetFold1.data._id,
    //         lang: signalGetFold1.data.lang,
    //         name: signalGetFold1.data?.name || "",
    //         title: signalGetFold1.data?.titles?.[0] || "",
    //         content: signalGetFold1.data?.contents?.[0] || "",
    //         description: signalGetFold1.data?.descriptions?.[0] || "",
    //         image: signalGetFold1.data?.images?.[0] || {},
    //         faq: signalGetFold1.data?.faq || [],
    //     }
    // }

    // getDataFaqCateSEOByPageName = async (page, name, lang, populate) => {
    //     const signalGetFold1 = await this.get(
    //         {
    //             page: { $in: [page] },
    //             name: { $in: [name] },
    //             lang,
    //             parent: null,
    //         },
    //         populate,
    //     );
    //     const data = formatDataSEO(signalGetFold1.data)
    //     return data
    // }
}

module.exports = new PageComponentService();