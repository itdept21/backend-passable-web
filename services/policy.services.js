const BaseService = require('./base.services');
const model = require('../models/policy.model');

class PolicyService extends BaseService {
    constructor() {
        super(model);
    }
}

module.exports = new PolicyService();