// @ts-nocheck
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var bodyParser = require('body-parser');
const useragent = require('express-useragent');

var cors = require('cors')
const app = express();
app.use(cookieParser());
app.use(cors())
// app.use(function (req, res, next) {
// 	res.header("Access-Control-Allow-Origin", "*");
// 	next();
// });

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");
app.use(require("passport").initialize());
app.use(bodyParser.json({ limit: '30mb', extended: true }))
app.use(bodyParser.urlencoded({ limit: '30mb', extended: true }))
// app.use(express.json());
// app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(useragent.express());

var indexRouter = require("./routes/index");
var adminRouter = require("./routes/admin");

app.use("/admin", adminRouter);
app.use("/", indexRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	res.status(404).json({ error: true, message: 'not found' })
});

// error handler
app.use(function (err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get("env") === "development" ? err : {};
	// if (err.status == 404) res.sendStatus(404);
	return res.send("error " + res.locals.message);
});

module.exports = app;
