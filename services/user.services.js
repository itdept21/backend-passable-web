// @ts-nocheck
const model = require("../models/admin.model");
const BaseService = require("./base.services");
const bcrypt = require("bcrypt");

const { RESPONSE_DATA, MESSAGES } = require("../constants/base.constants");

class UserService extends BaseService {
	constructor() {
		super(model);
	}

	/**
	 * @override
	 * @param {*} dataCreate
	 * @returns
	 */
	createService = async (dataCreate) => {
		try {
			const { username = "", password = "", role = 0, name = "", email = "", phone = "" } = dataCreate;
			const user = new this.model({ username, role, name, email, phone });
			user.setPassword(password);
			const result = await user.save();
			return RESPONSE_DATA({ data: result, message: MESSAGES.CREATED_SUCCEED });
		} catch (error) {
			return RESPONSE_DATA({ error: true, message: error.message });
		}
	};

	updatePassword = async (userID, password) => {
		try {
			password = bcrypt.hashSync(password, 10);
			const result = await this.model.updateOne({ _id: userID }, { $set: { password } });
			return RESPONSE_DATA({ data: result, message: MESSAGES.CREATED_SUCCEED });
		} catch (error) {
			return RESPONSE_DATA({ error: true, message: error.message });
		}
	};
}

module.exports = new UserService();
