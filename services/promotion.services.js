const model = require("../models/promotion.model");
const BaseService = require("./base.services");

class promotionService extends BaseService {
	constructor() {
		super(model);
	}
}

module.exports = new promotionService();
