const BaseService = require('./base.services');
const model = require('../models/services.model');

class ServicesService extends BaseService {
    constructor() {
        super(model);
    }
}

module.exports = new ServicesService();