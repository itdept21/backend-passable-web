var express = require("express");
var router = express.Router();

const controller = require("../controllers/servicesCategory.controller.js");
const { parseFormData } = require("../helpers/index.js");
const { GuardController } = require("../controllers/index.js");
const viewController = require("../controllers/view/servicesCategory.controller.js");

// // -------------------- GET DATA - RENDER VIEW ----------------------------
router.get("/service-categories/:id", GuardController.needLogin, viewController.renderEdit);
router.get("/service-categories", GuardController.needLogin, viewController.renderList);
router.get("/service-categories-duplicate/:id", GuardController.needLogin, viewController.duplicate);

// // -------------------- CREATE OR UPDATE ----------------------------
router.post("/service-categories", GuardController.needLogin, parseFormData, controller.createID);
router.post("/service-categories/quick-update/:id", GuardController.needLogin, parseFormData, controller.quickUpdate);

router.put('/service-categories/:id', GuardController.needLogin, parseFormData, controller.createOrUpdate);
router.put('/service-categories/:id/status/:status', GuardController.needLogin, parseFormData, controller.updateStatus);

// // -------------------- DELETE ----------------------------
router.delete("/service-categories/:id", GuardController.needLogin, controller.remove);

module.exports = router;

