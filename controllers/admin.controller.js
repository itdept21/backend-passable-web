
class AdminController {
	static refillPassword(req, res, next) {
		if (!req.body.password) {
			req.body.password = "   ";
		}
		return next();
	}

	static async renderLoginPage(req, res, next) {
		return res.render("login", { title: "TRANG QUẢN TRỊ ADMIN ", wrong: false });
	}

	static async destroySession(req, res, next) {
		req.logout();
		req.session.destroy();
		return res.redirect("/admin");
	}

	static async renderAdminDashboard(req, res, next) {
		const lang = req.query.lang || "vi";

		const totalPosts = 0

		return res.render("admin", {
			inc: "inc/admin/dashboard",
			title: "Dashboard ",
			lang,
			totalPosts,
		});
	}

	static async renderUploadSeoFiles(req, res, next) {
		res.render("admin", { inc: "inc/admin/public_file", title: "Upload Seo Files" });
	};

	static async updatePublicFile(req, res, next) {
		if (req.file) {
			return res.status(200).json({ error: false, message: "updated", data: true });
		}
		return res.status(415).json({ error: true, message: "cannot_upload_file" });
	};
}

module.exports = AdminController;
