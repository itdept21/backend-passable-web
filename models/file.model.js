const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const fileSchema = new Schema({
	name: { type: String, require: true },

	src: { type: String, required: true },

	type: { type: String, },

	// 1: file rac
	// 2: file da xu ly
	status: { type: Number, default: 1 },

	createdAt: { type: Date, require: true, default: new Date() },

	modifiedAt: { type: Date, require: true, default: new Date() },
});

const FILES_MODEL = mongoose.model("file", fileSchema);

module.exports = FILES_MODEL;
