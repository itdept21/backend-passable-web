const model = require("../models/paymentMethod.model");
const BaseService = require("./base.services");

class PaymentService extends BaseService {
	constructor() {
		super(model);
	}
}

module.exports = new PaymentService();
