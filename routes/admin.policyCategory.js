var express = require("express");
var router = express.Router();

const controller = require("../controllers/policyCategory.controller.js");
const { parseFormData } = require("../helpers/index.js");
const { GuardController } = require("../controllers/index.js");
const viewController = require("../controllers/view/policyCategory.controller.js");

// // -------------------- GET DATA - RENDER VIEW ----------------------------
router.get("/policy-categories/:id", GuardController.needLogin, viewController.renderEdit);
router.get("/policy-categories", GuardController.needLogin, viewController.renderList);


// // -------------------- CREATE OR UPDATE ----------------------------
router.post("/policy-categories", GuardController.needLogin, parseFormData, controller.createID);
router.put('/policy-categories/:id', GuardController.needLogin, parseFormData, controller.createOrUpdate);

router.put('/policy-categories/:id/status/:status', GuardController.needLogin, parseFormData, controller.updatePolicyCategoryStatus);

// // -------------------- DELETE ----------------------------
router.delete("/policy-categories/:id", GuardController.needLogin, controller.remove);

module.exports = router;

