const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ServicesSchema = new Schema({
	lang: { type: String, enum: ["vi", "en"] },

	name: { type: String, require: false, trim: true },
	description: { type: String, require: false, trim: true },
	content: { type: String, require: false, trim: true },
	slug: { type: String, require: false, index: { unique: true } },
	category: { type: mongoose.SchemaTypes.ObjectId, ref: "services_category" },
	thumbnail: { type: mongoose.Types.ObjectId, ref: "image", require: false },

	orderCount: { type: Number, default: 0 },
	totalViews: { type: Number, default: 0 },

	// recommend
	// 1: recommend
	// 0: not recommend
	recommend: { type: Number, default: 0 },

	// loại service
	typeOfServices: { type: String, default: "" },

	/////////////////
	// chỗ này sẽ hiển thị title mô tả rõ hơn cho giá tiền
	// Ví dụ: "Giá 10 sản phẩm là:"
	titleRate: { type: String, default: "" },
	// giá hiện tại
	newRate: { type: Number, default: 0 }, // giá bán
	// giá cũ
	oldRate: { type: Number, default: 0 }, // giá khuyến mãi 
	// word_count
	wordCount: { type: Number, default: 0 },

	////////////////

	/*** STATUS
	 * 2 - public, index
	 * 1 - public, noindex
	 * 0 - private
	 ***/
	status: { type: Number, require: false },

	buttonName: { type: String, trim: true },
	buttonLink: { type: String, trim: true },

	platform: { type: String, trim: true },

	createdAt: { type: Date, require: false, default: new Date() },
	createdBy: { type: String, require: false, default: null },
	modifiedAt: { type: Date, require: false, default: new Date() },
	modifiedBy: { type: String, require: false, default: null },

	/* SEO meta */
	seo_title: { type: String, trim: true },
	seo_description: { type: String, trim: true },
	seo_keywords: { type: String, trim: true },
	seo_schema: { type: String, trim: true },
	seo_canonical: { type: String, required: false },
	seo_redirect: { type: String, required: false },
	seo_lang: { type: String, required: false },
	seo_image: { type: mongoose.Types.ObjectId, ref: "image", require: false },
});

ServicesSchema.index({ title: "text", slug: "text", description: "text" });

module.exports = mongoose.model("service", ServicesSchema);
