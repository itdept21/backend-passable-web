// @ts-nocheck
const service = require("../services/pageComponent.services");
const BaseCrudController = require("./baseCrud.controller");
const { STATUS_CODE, } = require("../constants/base.constants");
const mongoose = require('mongoose');

class PageComponentController extends BaseCrudController {
	constructor(service) {
		super(service);
	}

	createOrUpdate = async (req, res, next) => {
		try {
			let {
				page, name, lang,
				title = "",
				content = "",
				description = "",
				url = '',

				images = [],
				serviceCategory = [],
				packages = [],
				faqs = [],
				feedbacks = [],

				opt = [],
				opt1,
				opt2,
				opt3,

				childTitles = [],
				childContents = [],
				childDescriptions = [],
				childUrls = [],
				childImages = [],
				childServiceCategory = [],
				childPackages = [],
				childFaqs = [],
				childFeedbacks = [],
				childOpt = [],
				childOpt1 = [],
				childOpt2 = [],
				childOpt3 = [],
				childButtonName = [],
				childButtonLink = [],

				seo_canonical, seo_schema, seo_title, seo_description, seo_h1, seo_lang, seo_keywords,
				createdAt,
				createdBy,
				buttonName,
				buttonLink,

				opt1Name,
				opt1LogoId,
				opt1LogoSrc,

				opt1Title,
				opt1Description,
				opt1Content,

			} = req.body;
			console.log(" req.body", req.body)

			if (!Array.isArray(opt)) opt = [opt];
			if (!Array.isArray(images)) images = [images];

			if (!Array.isArray(serviceCategory)) serviceCategory = [serviceCategory];
			if (!Array.isArray(packages)) packages = [packages];
			if (!Array.isArray(faqs)) faqs = [faqs];
			if (!Array.isArray(feedbacks)) feedbacks = [feedbacks];

			if (!Array.isArray(childTitles)) childTitles = [childTitles];
			if (!Array.isArray(childContents)) childContents = [childContents];
			if (!Array.isArray(childDescriptions)) childDescriptions = [childDescriptions];
			if (!Array.isArray(childUrls)) childUrls = [childUrls];
			if (!Array.isArray(childImages)) childImages = [childImages];
			if (!Array.isArray(childPackages)) childPackages = [childPackages];
			if (!Array.isArray(childFaqs)) childFaqs = [childFaqs];
			if (!Array.isArray(childFeedbacks)) childFeedbacks = [childFeedbacks];
			if (!Array.isArray(childOpt)) childOpt = [childOpt];
			if (!Array.isArray(childOpt1)) childOpt1 = [childOpt1];
			if (!Array.isArray(childOpt2)) childOpt2 = [childOpt2];
			if (!Array.isArray(childOpt3)) childOpt3 = [childOpt3];
			if (!Array.isArray(childButtonName)) childButtonName = [childButtonName];
			if (!Array.isArray(childButtonLink)) childButtonLink = [childButtonLink];

			opt1 = {
				name: opt1Name || "",
				title: opt1Title || "",
				content: opt1Content || "",
				description: opt1Description || "",
				logo: {
					src: opt1LogoSrc || "",
					_id: opt1LogoId || "",
				},
			};

			const _id = new mongoose.Types.ObjectId();

			const signalGet = await this.service.get({
				page,
				name,
				lang,
				parent: null,
			});

			let _childIds = []
			if (signalGet.error !== true) {
				await this.service.removeMany(
					{
						conditions: { parent: signalGet?.data?._id || _id }
					}
				);

				// create child
				if (signalGet.data && signalGet.data._id && childTitles.length > 0) {
					const dataCreateChild = childTitles.map((value, index) => {
						const _idChild = new mongoose.Types.ObjectId();

						const _titles = childTitles[index] || "";
						const _contents = childContents[index] || "";
						const _descriptions = childDescriptions[index] || "";
						const _url = childUrls[index] || "";
						const _images = childImages[index] || null;

						const _serviceCategory = childServiceCategory[index] || null;
						const _packages = childPackages[index] || null;
						const _faqs = childFaqs[index] || null;
						const _feedbacks = childFeedbacks[index] || null;

						const _opt = childOpt[index] || null;
						const _opt1 = childOpt1[index] || null;
						const _opt2 = childOpt2[index] || null;
						const _opt3 = childOpt3[index] || 0;

						const _buttonName = childButtonName[index] || null;
						const _buttonLink = childButtonLink[index] || null;

						const createdAt = new Date();
						const createdBy = req.user.username;

						return {
							_id: _idChild,
							page, name, lang,

							title: _titles,
							content: _contents,
							description: _descriptions,
							url: _url,
							images: _images,

							serviceCategory: _serviceCategory,
							packages: _packages,
							faqs: _faqs,
							feedbacks: _feedbacks,

							opt: _opt,
							opt1: _opt1,
							opt2: _opt2,
							opt3: _opt3,

							buttonName: _buttonName,
							buttonLink: _buttonLink,

							parent: signalGet?.data?._id || _id,
							createdAt, createdBy,
						};
					});
					await this.service.createMany(dataCreateChild);
					_childIds = (dataCreateChild.map(e => e._id))
				}
			};

			const dataUpdate = {
				seo_canonical, seo_schema, seo_title, seo_description, seo_h1, seo_lang, seo_keywords,
				page,
				name,
				lang,
				title: title,
				content: content,
				description: description,
				url: url,
				images: images.filter((e) => (e ? true : false)),

				serviceCategory: serviceCategory.filter((e) => (e ? true : false)),
				packages: packages.filter((e) => (e ? true : false)),
				faqs: faqs.filter((e) => (e ? true : false)),
				feedbacks: feedbacks.filter((e) => (e ? true : false)),

				opt: opt.filter((e) => (e ? true : false)),
				opt1,
				opt2,
				opt3,

				parent: null,
				child: _childIds, // tạo child mới
				buttonName,
				buttonLink,
				_id: signalGet?.data?._id || _id
			};
			if (!createdAt) dataUpdate.createdAt = new Date();
			if (!createdBy) dataUpdate.createdBy = req.user.username;
			dataUpdate.modifiedBy = req.user.username;

			const signalResponse = await this.service.createOrUpdate(
				{ page, name, lang, parent: null, }
				, dataUpdate
			);

			const signalResult = await this.service.get({
				page,
				name,
				lang,
				parent: null,
			});
			console.log("signalResult", signalResult)


			return res.status(signalResponse.error ? STATUS_CODE.BAD_REQUEST : STATUS_CODE.OK).json(signalResult);
		} catch (error) {
			console.log("error", error)
			return error
		}

	};
}

module.exports = new PageComponentController(service);
