const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const paymentMethodSchema = new Schema({
    bankName: { type: String, require: true },

    accountNumber: { type: String },
    accountName: { type: String },
    swiftCode: { type: String },
    postCode: { type: String },
    email: { type: String },

    type: { type: String },

    // 1 hiện - 0 ẩn
    status: { type: Number, default: 1 },

    thumbnail: { type: mongoose.Types.ObjectId, ref: "image", require: false },
    qrCode: { type: mongoose.Types.ObjectId, ref: "image", require: false },

    createdAt: { type: Date, require: false, default: new Date() },
    createdBy: { type: String, require: false, default: null },
    modifiedAt: { type: Date, require: false, default: new Date() },
    modifiedBy: { type: String, require: false, default: null },
});

const PAYMENT_METHOD = mongoose.model("payment_method", paymentMethodSchema);

module.exports = PAYMENT_METHOD;
