// @ts-nocheck
const BaseCrudController = require("../baseCrud.controller");
const service = require("../../services/services.services");
const servicesCategory = require("../../services/servicesCategory.services");
const {
    PAGINATION,
    STATUS_CODE,
    FORMAT_ID_FN,
} = require("../../constants/base.constants");
const { DATE_FORMAT_FN } = require("../../helpers/dateTimeFormat");
const {
    LIST_STATUS_EN,
    TYPE_OF_SERVICES,
} = require("../../constants/services.constants");
const CURRENCY_FORMAT_FN = require("../../helpers/currencyFormat");
const mongoose = require('mongoose');


class ViewServicesCategoryController extends BaseCrudController {
    constructor() {
        super(service);
    }

    renderList = async (req, res, next) => {
        const { page = 1, search = "", ...filter } = req.query;

        Object.entries(filter).forEach(([key, value]) => {
            if (!filter[key]) {
                delete filter[key];
            }
        });

        const url = req._parsedOriginalUrl.pathname;
        const oldQuery = req._parsedOriginalUrl.search || "?";

        const status = parseInt(filter?.status) || null

        if (page < 0) return res.sendStatus(400);

        const limit = 15;
        const skip = (page - 1) * limit;

        const queryConditions = search ? { $text: { $search: search } } : {};
        const signalGetList = await this.service.getList({
            skip, limit,
            matchConditions: { ...queryConditions, ...filter },
            populates: ['category', 'thumbnail'],
            sort: { modifiedAt: -1, createdAt: -1 }
        });

        const signalGetCount = await this.service.count({ ...queryConditions, ...filter });

        const total = signalGetCount.data;
        const pagination = PAGINATION(total, limit, url, oldQuery);

        if (signalGetList.error) next();
        const services = signalGetList.data;

        // list status
        const listStatus = LIST_STATUS_EN

        const listTypeOfServices = TYPE_OF_SERVICES;

        const signalGetServiceCategory = await servicesCategory.getList({
            matchConditions: {
                parent: { $ne: null },
                status: { $in: [1, 2] },
            },
            sort: { displayOrder: 1, parent: 1, modifiedAt: -1, createdAt: -1 }
        });
        const serviceCategory = signalGetServiceCategory.data || [];

        res.render("admin", {
            inc: "inc/admin/services",
            title: "Danh sách",
            services,
            listCategories: serviceCategory,
            pagination,
            page,
            total,
            search,
            listStatus, status,
            listTypeOfServices,
            filter,
            FORMAT_ID_FN,
            DATE_FORMAT_FN,
            CURRENCY_FORMAT_FN,

        });
    };

    duplicate = async (req, res, next) => {
        const { id } = req.params;
        const signalGet = await this.service.get({ _id: id.trim() }, ["thumbnail"]);
        const result = signalGet?.data || { _id: id }

        const _id = new mongoose.Types.ObjectId();
        const _name = result.name + " [copy" + "-" + DATE_FORMAT_FN(new Date(), 'HH:mm DD/MM/YYYY') + "]";
        const _slug = result.slug + "-copy" + "-" + DATE_FORMAT_FN(new Date(), 'HH-mm-DD-MM-YYYY');

        const dataDuplicate = {
            ...result,
            name: _name,
            slug: _slug,
            _id: _id,
        };
        await this.service.createOrUpdate(
            { _id: _id },
            dataDuplicate
        );
        res.setHeader("Content-Type", "text/html");
        res.redirect('/admin/services');
    }

    renderEdit = async (req, res, next) => {
        try {
            const { id } = req.params;
            const lang = req.query.lang || "vi";
            const switchLangs = {
                vi: `/admin/services/${id}?lang=vi`,
                en: `/admin/services/${id}?lang=en`,
            };

            const signalGet = await this.service.get({ _id: id.trim() }, ["thumbnail"]);
            const result = signalGet?.data || { _id: id }

            const signalGetCategories = await servicesCategory.getList(
                {
                    matchConditions:
                    {
                        parent: { $ne: null },
                        status: { $in: [1, 2] },
                    }
                }
            );
            if (signalGetCategories.error) return res.status(STATUS_CODE.SERVER_ERROR).json(signalGetCategories);
            const categories = signalGetCategories.data;

            return res.render("admin", {
                inc: "inc/admin/service",
                title: "Chỉnh sửa danh mục",
                service: result,
                lang,
                switchLangs,
                categories,
                CURRENCY_FORMAT_FN,
                TYPE_OF_SERVICES,
            });
        } catch (error) {
            return res.status(STATUS_CODE.SERVER_ERROR).json({ error: true, message: error.message });
        }
    };
}

module.exports = new ViewServicesCategoryController();
