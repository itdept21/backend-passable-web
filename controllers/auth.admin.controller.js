const { parseToken } = require('../helpers/authentication.helper');
const MODEL = require('../models/admin.model');

const { STATUS_CODE, MESSAGES } = require('../constants/base.constants');

const SERVICE = require('../services/admin.service');

async function adminAuthenticated(req, res, next) {
    const bearerToken = req.headers.authorization || req.headers.Authorization
    if (!bearerToken) return res.status(STATUS_CODE.UNAUTHORIZED).send('UNAUTHORIZED REJECTION');

    const [bearer = '', token = ''] = bearerToken.split(' ');
    if (!token || bearer !== 'Bearer') return res.status(STATUS_CODE.BAD_REQUEST).send('BAD REQUEST WITH BAD AUTHORIZATION TOKEN');

    const decodedToken = parseToken(token);

    if (!decodedToken) return res.status(STATUS_CODE.FORBIDDEN).send('TOKEN EXPIRATION REJECT');

    if (!decodedToken.adminID) return res.status(STATUS_CODE.FORBIDDEN).send(MESSAGES.PERMISSION_DENIED);

    const existAdmin = await MODEL.findOne({ _id: decodedToken.adminID }).lean();

    if (!existAdmin) return res.status(403).send('ADMIN NOT EXIST OR HAS BEEN REMOVED');

    req.admin = existAdmin;
    return next();
}

async function create(req, res, next) {

    //if (!req.admin) return res.status(STATUS_CODE.UNAUTHORIZED).send(MESSAGES.UNAUTHORIZED_ADMIN);

    // nếu chặn quyền admin thì viết ở đây

    const { password = '', fullname = '', username = '', role = 0 } = req.body

    const signalResponse = await SERVICE.create({ pass: password, fullname, username, role });
    const code = signalResponse.error ? STATUS_CODE.BAD_REQUEST : STATUS_CODE.OK;
    return res.status(code).json(signalResponse);
}

async function login(req, res, next) {
    const signalResponse = await SERVICE.login(req.body);
    const code = signalResponse?.error ? STATUS_CODE.NON_AUTH_INFO : STATUS_CODE.OK;
    return res.status(code).json(signalResponse);
}


module.exports = { adminAuthenticated, create, login };