const model = require("../models/contactOrder.model");
const BaseService = require("./base.services");

class ContactOrderService extends BaseService {
	constructor() {
		super(model);
	}
}

module.exports = new ContactOrderService();
