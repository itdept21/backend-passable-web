var express = require("express");
var router = express.Router();

const controller = require("../controllers/faq.controller.js");
const { parseFormData } = require("../helpers/index.js");
const { GuardController } = require("../controllers/index.js");
const viewController = require("../controllers/view/faq.controller.js");

// // -------------------- GET DATA - RENDER VIEW ----------------------------
router.get("/faqs/:id", GuardController.needLogin, viewController.renderEdit);
router.get("/faqs", GuardController.needLogin, viewController.renderList);

// // -------------------- CREATE OR UPDATE ----------------------------
router.post("/faqs", GuardController.needLogin, parseFormData, controller.createID);
router.post("/faqs/quick-update/:id", GuardController.needLogin, parseFormData, controller.quickUpdate);
router.put('/faqs/:id', GuardController.needLogin, parseFormData, controller.createOrUpdate);
router.put('/faqs/:id/status/:status', GuardController.needLogin, parseFormData, controller.updateFaqStatus);

// // -------------------- DELETE ----------------------------
router.delete("/faqs/:id", GuardController.needLogin, controller.remove);

module.exports = router;

