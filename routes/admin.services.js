var express = require("express");
var router = express.Router();

const controller = require("../controllers/services.controller.js");
const { parseFormData } = require("../helpers/index.js");
const { GuardController } = require("../controllers/index.js");
const viewController = require("../controllers/view/services.controller.js");

// // -------------------- GET DATA - RENDER VIEW ----------------------------
router.get("/services/:id", GuardController.needLogin, viewController.renderEdit);
router.get("/services", GuardController.needLogin, viewController.renderList);
router.get("/services-duplicate/:id", GuardController.needLogin, viewController.duplicate);

// // -------------------- CREATE OR UPDATE ----------------------------
router.post("/services", GuardController.needLogin, parseFormData, controller.createID);
router.post("/services/quick-update/:id", GuardController.needLogin, parseFormData, controller.quickUpdate);

router.put('/services/:id', GuardController.needLogin, parseFormData, controller.createOrUpdate);
router.put('/services/:id/status/:status', GuardController.needLogin, parseFormData, controller.updateStatus);

// // -------------------- DELETE ----------------------------
router.delete("/services/:id", GuardController.needLogin, controller.remove);

module.exports = router;

