const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const model = new Schema({
	lang: { type: String, enum: ["vi", "en"], default: "en" },
	code: { type: String },
	name: { type: String },
	description: { type: String },

	icon: { type: mongoose.Types.ObjectId, ref: "image", require: true },

	// số tiền giảm - hoặc số % giảm dựa vào type
	price: { type: Number, default: 0 },
	minRateOrder: { type: Number, default: 0 },

	/* 
		0 - mã giảm giá %
		1 - mã giảm tiền trực tiếp
	*/
	type: { type: Number, default: 0 },

	// Ngày hết hạn mã
	dateExpired: { type: Date, default: null },

	// 1 hiện - 0 ẩn
	status: { type: Number, default: 1 },

	createdAt: { type: Date, require: false, default: new Date() },
	createdBy: { type: String, require: false, default: null },
	modifiedAt: { type: Date, require: false, default: new Date() },
	modifiedBy: { type: String, require: false, default: null },
});

const NEW_MODEL = mongoose.model("promotions", model);

module.exports = NEW_MODEL;
