// @ts-nocheck
const BaseCrudController = require("./baseCrud.controller");
const service = require("../services/servicesCategory.services");
const {
	STATUS_CODE,
	MESSAGES,
	RESPONSE_DATA,
} = require("../constants/base.constants");

class ServicesCategoryController extends BaseCrudController {
	constructor() {
		super(service);
	}

	createOrUpdate = async (req, res, next) => {
		try {
			let id = req.params.id.trim();
			let {
				name, lang, description, content,
				thumbnail, slug, typeOfServices,
				status, parent, displayOrder,
				buttonName, buttonLink,
				platform,
				seo_title, seo_description, seo_keywords, seo_schema, seo_canonical, seo_redirect, seo_lang, seo_image,
				createdAt, createdBy,
				modifiedAt, modifiedBy,
			} = req.body;

			if (!createdAt) createdAt = new Date();
			if (!createdBy) createdBy = req.user.username;
			modifiedBy = req.user.username;

			thumbnail = thumbnail ? thumbnail : null;
			parent = parent ? parent : null;
			seo_image = seo_image ? seo_image : null;

			const dataUpdate = {
				name, lang, description, content,
				thumbnail, slug, typeOfServices,
				status, parent, displayOrder,
				buttonName, buttonLink,
				platform,
				seo_title, seo_description, seo_keywords, seo_schema, seo_canonical, seo_redirect, seo_lang, seo_image,
				createdAt, createdBy,
				modifiedAt, modifiedBy,
			};
			const signalUpdate = await this.service.createOrUpdate({ _id: id }, dataUpdate);

			if (signalUpdate.error) return res.status(STATUS_CODE.SERVER_ERROR).json(signalUpdate);
			let dataResult = signalUpdate.data;

			const responseJson = RESPONSE_DATA(
				{
					data: dataResult,
					message: MESSAGES.CREATED_SUCCEED
				}
			);
			return res.status(STATUS_CODE.OK).json(responseJson);
		} catch (error) {
			console.log("error", error)
			return res.status(400).send(RESPONSE_DATA({ error: true, message: error.message }));
		}
	};

	quickUpdate = async (req, res, next) => {
		const { id } = req.params;
		const { name, displayOrder, seo_title, seo_description } = req.body;
		const modifiedBy = req.user.username;

		const signalGetService = await this.service.get({ _id: id });

		const service = signalGetService.data;

		if (!service) return res.status(400).json(signalGetService)
		// quick update
		const signalQuickUpdate = await this.service.update(
			{
				_id: id
			},
			{
				name,
				displayOrder,
				modifiedBy,
				seo_title,
				seo_description,
			});

		if (signalQuickUpdate.error) return res.status(STATUS_CODE.SERVER_ERROR).json(signalQuickUpdate);

		return res.status(STATUS_CODE.OK).json(signalQuickUpdate);
	}

	updateStatus = async (req, res, next) => {
		let { id, status } = req.params;
		status = parseInt(status);
		const modifiedBy = req.user.username;

		if ([0, 1, 2].indexOf(parseInt(status)) < 0)
			return res.status(STATUS_CODE.BAD_REQUEST).json(RESPONSE_DATA({ error: true, message: "invalid_status" }));

		const signalUpdate = await this.service.update({ _id: id }, { status, modifiedBy });

		if (signalUpdate.error) return res.status(STATUS_CODE.SERVER_ERROR).json(signalUpdate);

		return res.status(STATUS_CODE.OK).json(signalUpdate);
	}

	remove = async (req, res, next) => {
		const { id } = req.params;

		const signalGetResult = await this.service.get({ _id: id });
		if (signalGetResult.error) res.status(STATUS_CODE.BAD_REQUEST).json(signalGetResult);

		const result = signalGetResult.data;

		const removeSignals = await Promise.all([
			this.service.remove(result._id),
		]);

		const data = removeSignals.data
		return res.status(STATUS_CODE.OK).json(RESPONSE_DATA({ data, message: MESSAGES.DELETE_SUCCEED }));
	};
}

module.exports = new ServicesCategoryController();
