var express = require("express");
var router = express.Router();
const Controller = require("../controllers/paymentMethod.controller");
const GuardController = require("../controllers/guard.controller");
const ViewController = require("../controllers/view/paymentMethod.controller");
const { parseFormData } = require("../helpers");

// render view
router.get("/payment-methods/:id", GuardController.needLogin, parseFormData, ViewController.renderEdit);
router.get("/payment-methods", GuardController.needLogin, parseFormData, ViewController.renderList);

router.post("/payment-methods", GuardController.needLogin, parseFormData, Controller.create);

router.put("/payment-methods/:id", GuardController.needLogin, parseFormData, Controller.createOrUpdate);

router.delete("/payment-methods/:id", GuardController.needLogin, parseFormData, Controller.remove);

module.exports = router;
