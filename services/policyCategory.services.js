const BaseService = require('./base.services');
const model = require('../models/policyCategory.model');

class PolicyCategoryService extends BaseService {
    constructor() {
        super(model);
    }
}

module.exports = new PolicyCategoryService();