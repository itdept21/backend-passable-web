// @ts-nocheck
const ComponentService = require("../services/pageComponent.services");
const PackageService = require("../services/services.services");
const ServiceCateService = require("../services/servicesCategory.services");
const FeedbackService = require("../services/feedback.services");
const FaqService = require("../services/faq.services");
const FaqCategoryService = require("../services/faqCategory.services");

class LayoutController {
    static async renderEditHomePage(req, res, next) {
        const lang = req.query.lang || "en";
        const switchLangs = {
            vi: `/admin/page-home?lang=vi`,
            en: `/admin/page-home?lang=en`,
        };
        const matchConditions = {
            page: { $in: ["home"] },
            lang,
            parent: null,
        };
        const skip = 0,
            limit = 20;
        const populates = [
            "images",
            "child",
            {
                path: "child",
                populate: {
                    path: "images",
                },
            },
        ];
        const signalResponse = await ComponentService.getList({ matchConditions, skip, limit, populates });

        // faqs cate home
        const signalGetFaqCateHome = await FaqCategoryService.getList({
            matchConditions: {
                lang,
                status: { $gte: 1 },
            },
        });

        // Faq Service
        const signalGetFaqService = await FaqService.getList({
            matchConditions: {
                // category: { $in: signalGetFaqCateHome.data.map((e) => e._id) },
                lang,
                status: { $gte: 1 },
            },
        });
        console.log("signalGetFaqService", signalGetFaqService)

        // Services Cate Service
        const signalGetServiceCateService = await ServiceCateService.getList({
            matchConditions: {
                lang,
                parent: null,
                status: { $gte: 1 },
            },
            populates: [
                "images",
                "child",
                {
                    path: "child",
                    populate: {
                        path: "images",
                    },
                },
            ]
        });

        // Package Service
        const signalGetPackageService = await PackageService.getList({
            matchConditions: {
                lang,
                status: { $gte: 1 },
                typeOfServices: "package"
            },
        });

        // feedbacks Service
        const signalGetFeedbackService = await FeedbackService.getList({
            matchConditions: {
                lang,
                status: { $gte: 1 },
            },
        });

        res.render("admin", {
            inc: "inc/admin/page_home",
            title: "ADMIN - Home page",
            components: signalResponse.data,

            serviceCategory: signalGetServiceCateService.data || [],

            packages: signalGetPackageService.data || [],
            faqs: signalGetFaqService.data || [],
            feedbacks: signalGetFeedbackService.data || [],

            lang,
            switchLangs,
        });
    };

    static async renderEditAboutPage(req, res, next) {
        const lang = req.query.lang || "en";
        const switchLangs = {
            vi: `/admin/page-about?lang=vi`,
            en: `/admin/page-about?lang=en`,
        };
        const matchConditions = {
            page: { $in: ["about"] },
            lang,
            parent: null,
        };
        const skip = 0,
            limit = 20;
        const populates = [
            "images",
            "child",
            {
                path: "child",
                populate: {
                    path: "images",
                },
            },
        ];

        const signalResponse = await ComponentService.getList({ matchConditions, skip, limit, populates });

        res.render("admin", {
            inc: "inc/admin/page_about",
            title: "ADMIN - About page",
            components: signalResponse.data,
            lang,
            switchLangs,
        });
    };

    static async renderSeoPage(req, res, next) {
        const { lang = "en" } = req.query;
        const { pageName } = req.params;

        const pageNames = {
            home: "Home",
            about: "About",
            policy: "Policy",
            faq: "faq",
            service: "service",
        };

        const subTitle = pageNames[pageName] || null;
        if (!subTitle) res.sendStatus(400);

        const switchLangs = {
            vi: `/admin/seo/${pageName}?lang=vi`,
            en: `/admin/seo/${pageName}?lang=en`,
        };

        const matchConditions = { page: { $in: [pageName] }, lang };
        const skip = 0,
            limit = 100;
        const populates = ["images"];

        const signalResponse = await ComponentService.getList({ matchConditions, skip, limit, populates });

        return res.render("admin", {
            inc: "inc/admin/seo",
            title: "SEO - " + subTitle,
            lang,
            switchLangs,
            components: signalResponse.data,
            pageName,
        });
    };
}

module.exports = LayoutController;
