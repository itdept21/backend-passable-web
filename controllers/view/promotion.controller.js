// @ts-nocheck
const BaseCrudController = require("../baseCrud.controller");
const service = require("../../services/promotion.services");
const { PAGINATION, FORMAT_ID_FN, STATUS_CODE } = require("../../constants/base.constants");
const { DATE_FORMAT_FN } = require("../../helpers/dateTimeFormat");

class ViewStoreLocationController extends BaseCrudController {
  constructor() {
    super(service);
  }

  renderList = async (req, res, next) => {
    const { page = 1, search = "" } = req.query;

    const url = req._parsedOriginalUrl.pathname;
    const oldQuery = req._parsedOriginalUrl.search || "?";

    if (page < 0) return res.sendStatus(400);
    const limit = 15;
    const skip = (page - 1) * limit;

    const conditions = search ? { $text: { $search: search } } : {};

    const signalGetList = await this.service.getList({
      skip,
      limit,
      sort: { modifiedAt: -1, createdAt: -1 },
      matchConditions: conditions,
    });
    const signalGetCount = await this.service.count(conditions);

    const total = signalGetCount.data;
    const pagination = PAGINATION(total, limit, url, oldQuery);

    if (signalGetList.error) next();
    const promotions = signalGetList.data;
    res.render("admin", {
      inc: "inc/admin/promotions",
      title: "Danh sách mã khuyến mãi",
      promotions,
      pagination,
      page,
      total,
      search,
      FORMAT_ID_FN,
      DATE_FORMAT_FN
    });
  };

  renderEdit = async (req, res, next) => {
    const { id } = req.params;

    const signalGetPromotion = await this.service.get({ _id: id.trim() }, ['icon']);

    if (signalGetPromotion.error)
      return res.sendStatus(STATUS_CODE.BAD_REQUEST);
    const promotion = signalGetPromotion.data;


    if (!promotion) return next();

    return res.render("admin", {
      inc: "inc/admin/promotion",
      title: "Chỉnh sửa mã khuyến mãi",
      promotion,
      DATE_FORMAT_FN
    });
  };
}

module.exports = new ViewStoreLocationController();
