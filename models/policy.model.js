const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const policySchema = new Schema({
	lang: { type: String, enum: ["vi", "en"] },

	name: { type: String, require: false, trim: true },

	description: { type: String, require: false, trim: true },

	content: { type: String, require: false, trim: true },

	thumbnail: { type: mongoose.Types.ObjectId, ref: "image", require: false },

	slug: { type: String, require: false, index: { unique: true } },

	category: { type: mongoose.SchemaTypes.ObjectId, ref: "policy_category" },

	/*** STATUS
	 * 2 - public, index
	 * 1 - public, noindex
	 * 0 - private
	 ***/
	status: { type: Number, require: false },

	toc: { type: String, require: false },

	menuList: [{ type: Schema.Types.Mixed, require: false }],

	// vị trí hiển thị
	displayOrder: { type: Number, require: false },

	createdAt: { type: Date, require: false, default: new Date() },
	createdBy: { type: String, require: false, default: null },
	modifiedAt: { type: Date, require: false, default: new Date() },
	modifiedBy: { type: String, require: false, default: null },

	/* SEO meta */
	seo_title: { type: String, trim: true },
	seo_description: { type: String, trim: true },
	seo_keywords: { type: String, trim: true },
	seo_schema: { type: String, trim: true },
	seo_canonical: { type: String, required: false },
	seo_redirect: { type: String, required: false },
	seo_lang: { type: String, required: false },
	seo_image: { type: mongoose.Types.ObjectId, ref: "image", require: false },
});

policySchema.index({ title: "text", slug: "text", description: "text" });

module.exports = mongoose.model("policy", policySchema);
