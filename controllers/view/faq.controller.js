// @ts-nocheck
const BaseCrudController = require("../baseCrud.controller");
const service = require("../../services/faq.services");
const serviceCategory = require("../../services/faqCategory.services");
const { PAGINATION, STATUS_CODE, FORMAT_ID_FN, LIST_STATUS } = require("../../constants/base.constants");
const { DATE_FORMAT_FN } = require("../../helpers/dateTimeFormat");

class ViewFaqController extends BaseCrudController {
    constructor() {
        super(service);
    }

    renderList = async (req, res, next) => {
        const { page = 1, search = "", ...filter } = req.query;

        const url = req._parsedOriginalUrl.pathname;
        const oldQuery = req._parsedOriginalUrl.search || "?";

        const status = parseInt(filter?.status) || null

        const category = filter?.category || ""
        if (!filter?.category) {
            delete filter?.category
        };
        if (!filter?.status) {
            delete filter?.status
        }

        if (page < 0) return res.sendStatus(400);

        const limit = 15;
        const skip = (page - 1) * limit;

        const queryConditions = search ? { $text: { $search: search } } : {};
        const signalGetList = await this.service.getList({
            skip, limit,
            matchConditions: { ...queryConditions, ...filter },
            populates: ['category', 'thumbnail', 'relatedFaq', 'seo_image'],
            sort: { displayOrder: -1, modifiedAt: -1, createdAt: -1 }
        });

        const signalGetCount = await this.service.count(
            {
                ...queryConditions,
                ...filter
            }
        );

        const total = signalGetCount.data;
        const pagination = PAGINATION(total, limit, url, oldQuery);

        if (signalGetList.error) next();
        const faqs = signalGetList.data;

        // list status
        const listStatus = LIST_STATUS;

        const signalGetCategory = await serviceCategory.getList({
            matchConditions: {
                status: { $gte: 1 },
            },
            sort: {
                displayOrder: 1,
                modifiedAt: -1,
                createdAt: -1
            }
        });
        const dataCategory = signalGetCategory.data || []


        res.render("admin", {
            inc: "inc/admin/faqs",
            title: "Danh sách",
            faqs,
            listCategory: dataCategory,
            category,
            pagination,
            page,
            total,
            search,
            listStatus,
            status,
            FORMAT_ID_FN,
            DATE_FORMAT_FN,
        });
    };

    renderEdit = async (req, res, next) => {
        try {
            const { id } = req.params;
            const lang = req.query.lang || "vi";
            const switchLangs = {
                vi: `/admin/faqs/${id}?lang=vi`,
                en: `/admin/faqs/${id}?lang=en`,
            };

            const signalGet = await this.service.get(
                {
                    _id: id.trim()
                },
                ["thumbnail", 'seo_image']
            );
            const result = signalGet?.data || { _id: id }

            const signalGetCategories = await serviceCategory.getList(
                {
                    matchConditions:
                    {
                        parent: null,
                        status: { $in: [1, 2] },
                    }
                }
            );
            if (signalGetCategories.error)
                return res.status(STATUS_CODE.SERVER_ERROR)
                    .json(signalGetCategories);

            const categories = signalGetCategories.data;

            const signalGetCatFaq = await this.service.getList({
                matchConditions: {
                    status: { $gte: 1 },
                },
                sort: {
                    displayOrder: 1,
                    modifiedAt: -1,
                    createdAt: -1
                }
            });
            const dataFaq = signalGetCatFaq.data || []

            return res.render("admin", {
                inc: "inc/admin/faq",
                title: "Chỉnh sửa danh mục",
                faq: result,
                listFaq: dataFaq,
                lang,
                switchLangs,
                categories,
                DATE_FORMAT_FN,
            });
        } catch (error) {
            return res.status(STATUS_CODE.SERVER_ERROR)
                .json({ error: true, message: error.message });
        }
    };
}

module.exports = new ViewFaqController();
