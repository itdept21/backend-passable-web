var express = require("express");
var router = express.Router();
const Controller = require("../controllers/promotion.controller");
const GuardController = require("../controllers/guard.controller");
const ViewController = require("../controllers/view/promotion.controller");
const { parseFormData } = require("../helpers");

// render view
router.get("/promotions/:id", GuardController.needLogin, parseFormData, ViewController.renderEdit);
router.get("/promotions", GuardController.needLogin, parseFormData, ViewController.renderList);

router.post("/promotions", GuardController.needLogin, parseFormData, Controller.create);

router.put("/promotions/:id", GuardController.needLogin, parseFormData, Controller.createOrUpdate);

router.delete("/promotions/:id", GuardController.needLogin, parseFormData, Controller.remove);

module.exports = router;
