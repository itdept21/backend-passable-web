const mongoose = require("mongoose");
const { RESPONSE_DATA, MESSAGES
} = require("../constants/base.constants");

class BaseService {
	constructor(model = mongoose.model()) {
		this.model = model;
	}

	/**
	 *
	 * @param {*} matchConditions Partial of Model Schema
	 * @param {*} populates       Model's ref populations
	 * @returns {SignalResponse}    Signal Response
	 */
	get = async (queryConditions = {}, populates = [], select) => {
		try {
			let queryPromisePipeline = this.model.findOne(queryConditions);


			populates.forEach((populate) => {
				queryPromisePipeline.populate(populate);
			});

			if (select) queryPromisePipeline.select(select);

			const data = await queryPromisePipeline.lean();

			// if (!data) return RESPONSE_DATA({ error: true, message: 'cannot_get_data' });

			return RESPONSE_DATA({ data, message: MESSAGES.GET_SUCCEED });
		} catch (error) {
			return RESPONSE_DATA({ error: true, message: error.message });
		}
	};

	/**
	 * Get list data from database
	 * @param {*} matchConditions   query conditions
	 * @param {*} populates         population refs array
	 * @param {*} skip              documents skip
	 * @param {*} limit             documents limit
	 * @param {*} sort              documents sort
	 * @param {*} select            document's field selections
	 * @returns {SignalResponse}
	 */
	getList = async ({ matchConditions = {}, populates = [], skip = 0, limit, sort = { modifiedAt: -1, createdAt: -1 }, select }) => {
		try {
			let queryPromisePipeline = this.model.find(matchConditions);
			populates.forEach((populate) => {
				queryPromisePipeline.populate(populate);
			});

			if (select) queryPromisePipeline.select(select);

			if (parseInt(limit)) {
				let _limit = parseInt(limit);
				if (_limit && _limit <= 0) return RESPONSE_DATA({ error: true, message: "invalid_limit" });
				if (_limit > 0) queryPromisePipeline.limit(_limit);
			}

			let _skip = parseInt(skip)
			if (_skip && _skip <= 0) return RESPONSE_DATA({ error: true, message: "invalid_skip" });
			if (_skip > 0) queryPromisePipeline.skip(_skip);

			if (sort !== {}) queryPromisePipeline.sort(sort);

			const result = await queryPromisePipeline.lean();
			return RESPONSE_DATA({ data: result, message: MESSAGES.GET_SUCCEED });
		} catch (error) {
			console.log(error);
			return RESPONSE_DATA({ error: true, message: error.message });
		}
	};

	// get list k có skip limit
	getListAll = async ({ matchConditions = {}, populates = [], sort = {}, select }) => {
		try {
			let queryPromisePipeline = this.model.find(matchConditions);
			populates.forEach((populate) => {
				queryPromisePipeline.populate(populate);
			});

			// if (typeof select !== "string" || !select.length) RESPONSE_DATA({ error: true, message: "invalid_select" });
			if (select) queryPromisePipeline.select(select);

			if (sort !== {}) queryPromisePipeline.sort(sort);

			const result = await queryPromisePipeline.lean();
			return RESPONSE_DATA({ data: result, message: MESSAGES.GET_SUCCEED });
		} catch (error) {
			console.log(error);
			return RESPONSE_DATA({ error: true, message: error.message });
		}
	};

	/**
	 * Create new or update an exists document
	 * @param {*} matchConditions identify conditions
	 * @param {*} dataUpdate      data update info
	 * @returns {SignalResponse}
	 */
	createOrUpdate = async (matchConditions = {}, dataUpdate = {}, populates) => {
		try {
			dataUpdate.modifiedAt = new Date();
			const updateResult = await this.model.findOneAndUpdate(
				matchConditions,
				{
					$set: dataUpdate
				},
				{ upsert: true, new: true }
			)
				.populate(populates)
				.lean();

			return RESPONSE_DATA({ data: updateResult, message: MESSAGES.CREATED_SUCCEED });
		} catch (error) {
			return RESPONSE_DATA({ error: true, message: error.message });
		}
	};

	createOrUpdateV2 = async (matchConditions = {}, dataUpdate = {}, increase = {}, populates,) => {
		try {
			const updateResult = await this.model.findOneAndUpdate(
				matchConditions,
				{
					$set: dataUpdate,
					$inc: increase
				},
				{ upsert: true, new: true }
			)
				.populate(populates)
				.lean();

			return RESPONSE_DATA({ data: updateResult, message: MESSAGES.CREATED_SUCCEED });
		} catch (error) {
			return RESPONSE_DATA({ error: true, message: error.message });
		}
	};

	createID = () => {
		return RESPONSE_DATA({ error: false, data: { _id: new mongoose.Types.ObjectId(), createdAt: new Date() }, message: 'created_empty' })
	}

	/**
	 * Insert new document with mongoose model schema
	 * @param {*} dataCreate  Partial of Schema_Model
	 * @returns {SignalResponse}
	 */
	create = async (dataCreate = {}) => {
		try {

			dataCreate.modifiedAt = new Date();
			dataCreate.createdAt = new Date();
			const createResult = await new this.model(dataCreate).save();
			return RESPONSE_DATA({ error: false, data: createResult, message: MESSAGES.CREATED_SUCCEED });
		} catch (error) {
			console.log(error);
			return RESPONSE_DATA({ error: true, message: error.message });
		}
	};

	createMany = async (payloads = {}) => {
		try {
			const docs = await this.model.insertMany(payloads);
			return RESPONSE_DATA({ error: false, data: docs, message: MESSAGES.CREATED_SUCCEED_MANY });
			//   return docs ;
		} catch (error) {
			console.log(error);
			return RESPONSE_DATA({ error: true, message: error.message });
		}
	}

	/**
	 * Delete a document by ID
	 * @param {*} id  ObjectID that identify the document
	 * @returns {SignalResponse}
	 */
	remove = async (id, callback = () => { }) => {
		try {
			const removeResult = await this.model.findOneAndDelete({ _id: id });
			await callback();
			return RESPONSE_DATA({ data: removeResult, message: MESSAGES.DELETE_SUCCEED });
		} catch (error) {
			return RESPONSE_DATA({ error: true, message: error.message });
		}
	};

	removeMany = async ({ conditions = {} }, callback = () => { }) => {
		try {
			const removeResult = await this.model.deleteMany(conditions);
			await callback();
			return RESPONSE_DATA({
				data: removeResult,
				message: MESSAGES.DELETE_SUCCEED,
			});
		} catch (error) {
			return RESPONSE_DATA({ error: true, message: error.message });
		}
	};

	/**
	 * Update a document following the dataUpdate object
	 * @param {*} matchConditions
	 * @param {*} dataUpdate
	 * @returns {SignalResponse}
	 */
	update = async (matchConditions = {}, dataUpdate = {}) => {
		try {
			dataUpdate.modifiedAt = new Date();
			const updateResult = await this.model.findOneAndUpdate(matchConditions, { $set: dataUpdate }, { new: true });
			// console.log(matchConditions, updateResult);
			return RESPONSE_DATA({ data: updateResult, message: MESSAGES.UPDATE_SUCCEED });
		} catch (error) {
			return RESPONSE_DATA({ error: true, message: error.message });
		}
	};

	/**
	 * Update documents following the dataUpdate object
	 * @param {*} matchConditions
	 * @param {*} dataUpdate
	 * @returns {SignalResponse}
	 */
	updateMany = async (matchConditions = {}, dataUpdate = {}) => {
		try {
			dataUpdate.modifiedAt = new Date();
			const updateResult = await this.model.updateMany(matchConditions, { $set: dataUpdate }, { new: true });
			// console.log(matchConditions, updateResult);
			return RESPONSE_DATA({ data: updateResult, message: MESSAGES.UPDATE_SUCCEED });
		} catch (error) {
			return RESPONSE_DATA({ error: true, message: error.message });
		}
	};

	/**
	 * Count matching conditions documents
	 * @param {*} matchConditions
	 * @returns {SignalResponse}
	 */
	count = async (matchConditions = {}) => {
		try {
			const result = await this.model.countDocuments(matchConditions);
			return RESPONSE_DATA({ data: result, message: MESSAGES.DELETE_SUCCEED });
		} catch (error) {
			return RESPONSE_DATA({ error: true, message: error.message });
		}
	};
}

module.exports = BaseService;
