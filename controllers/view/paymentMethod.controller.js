// @ts-nocheck
const BaseCrudController = require("../baseCrud.controller");
const service = require("../../services/paymentMethod.services");
const { PAGINATION, FORMAT_ID_FN, STATUS_CODE } = require("../../constants/base.constants");
const { DATE_FORMAT_FN } = require("../../helpers/dateTimeFormat");

class ViewStoreLocationController extends BaseCrudController {
  constructor() {
    super(service);
  }

  renderList = async (req, res, next) => {
    const { page = 1, search = "" } = req.query;

    const url = req._parsedOriginalUrl.pathname;
    const oldQuery = req._parsedOriginalUrl.search || "?";

    if (page < 0) return res.sendStatus(400);
    const limit = 15;
    const skip = (page - 1) * limit;

    const conditions = search ? { $text: { $search: search } } : {};

    const signalGetList = await this.service.getList({
      skip,
      limit,
      sort: { modifiedAt: -1, createdAt: -1 },
      matchConditions: conditions,
    });
    const signalGetCount = await this.service.count(conditions);

    const total = signalGetCount.data;
    const pagination = PAGINATION(total, limit, url, oldQuery);

    if (signalGetList.error) next();
    const paymentMethods = signalGetList.data;
    res.render("admin", {
      inc: "inc/admin/payment_methods",
      title: "Danh sách",
      paymentMethods,
      pagination,
      page,
      total,
      search,
      FORMAT_ID_FN,
      DATE_FORMAT_FN
    });
  };

  renderEdit = async (req, res, next) => {
    const { id } = req.params;
    console.log("renderEdit", id)
    const signalGetPaymentMethod = await this.service.get(
      {
        _id: id.trim()
      },
      ['thumbnail', 'qrCode']
    );

    if (signalGetPaymentMethod.error)
      return res.sendStatus(STATUS_CODE.BAD_REQUEST);
    const paymentMethod = signalGetPaymentMethod.data;

    if (!paymentMethod) return next();

    const signalGetList = await this.service.getList({
      matchConditions: {},
      sort: { modifiedAt: -1, createdAt: -1 },
    });
    let _categories = signalGetList?.data?.reduce((arr, item) => {
      const _tmp = arr.filter((e) => e.type == item.type);
      if (_tmp.length == 0)
        if (item.type !== undefined && item.type !== "" && item.type !== null) {
          arr.push({
            _id: item.type,
            name: item.type
          })
        }
      return arr;
    }, [])

    console.log("_categories", _categories)

    return res.render("admin", {
      inc: "inc/admin/payment_method",
      title: "Chỉnh sửa",
      paymentMethod,
      categories: _categories,
      DATE_FORMAT_FN
    });
  };
}

module.exports = new ViewStoreLocationController();
