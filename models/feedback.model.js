const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const model = new Schema({
    // thông tin chung customer, sẽ là model riêng
    customerInfo: { type: Schema.Types.Mixed, trim: true, default: null },

    // ratingValue: 0-5
    star: { type: Number, require: true, default: 0 },
    content: { type: String, trim: true, require: false },
    replyText: { type: String, trim: true, require: false },
    replyDate: { type: Date, require: false },

    service: { type: mongoose.Types.ObjectId, ref: "service", default: null },
    createdDate: { type: String, trim: true, require: false },
    note: { type: String, trim: true, require: false },

    result: {
        score: { type: String, trim: true },
        images: [{ type: mongoose.Types.ObjectId, ref: "image" }],
    },

    /*** STATUS
     * 	1 - Mở
     * 	0 - Chưa duyệt 	
     * -1 - khóa
    ***/
    status: { type: Number, require: false, default: 1 },

    lang: { type: String, trim: true, enum: ["vi", "en"], default: "en", required: true },

    createdAt: { type: Date, require: false, default: new Date() },
    createdBy: { type: String, require: false, default: null },
    modifiedAt: { type: Date, require: false, default: new Date() },
    modifiedBy: { type: String, require: false, default: null },
});

module.exports = mongoose.model("feedback", model);
