const session = require("express-session");
const mongoose = require("mongoose");
var MongoStore = require("connect-mongo")(session);

/**
 * @param {*} router Express Routers
 */
function register(router) {
    router.use(
        session({
            name: "session_id",
            secret: "myerw34o5hsfn498498y5209uonfaq", // Please put it to secret place :>
            resave: false,
            saveUninitialized: false,
            store: new MongoStore({ mongooseConnection: mongoose.connection }),
            cookie: { maxAge: 30 * 24 * 60 * 60 * 1000 },
        })
    );

}


module.exports = { register }