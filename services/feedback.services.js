const BaseService = require('./base.services');
const model = require('../models/feedback.model');

class FeedbackService extends BaseService {
    constructor() {
        super(model);
    }
}

module.exports = new FeedbackService();