const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const policyCategorySchema = new Schema({
	lang: { type: String, enum: ["vi", "en"] },

	name: { type: String, require: false, trim: true },

	description: { type: String, require: false, trim: true },

	content: { type: String, require: false, trim: true },

	thumbnail: { type: mongoose.Types.ObjectId, ref: "image", require: false },

	slug: { type: String, require: false, index: { unique: true } },

	/*** STATUS
	 * 2 - public, index
	 * 1 - public, noindex
	 * 0 - private
	 ***/
	status: { type: Number, require: false },

	// published day
	publishedAt: { type: Date, require: false },

	createdAt: { type: Date, require: false, default: new Date() },
	createdBy: { type: String, require: false, default: null },
	modifiedAt: { type: Date, require: false, default: new Date() },
	modifiedBy: { type: String, require: false, default: null },

	/* SEO meta */
	seo_title: { type: String, trim: true },
	seo_description: { type: String, trim: true },
	seo_keywords: { type: String, trim: true },
	seo_schema: { type: String, trim: true },
	seo_canonical: { type: String, required: false },
	seo_redirect: { type: String, required: false },
	seo_lang: { type: String, required: false },
	seo_image: { type: mongoose.Types.ObjectId, ref: "image", require: false },
});

policyCategorySchema.index({ title: "text", slug: "text", description: "text" });

module.exports = mongoose.model("policy_category", policyCategorySchema);
