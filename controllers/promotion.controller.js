// @ts-nocheck
const BaseCrudController = require("./baseCrud.controller");
const service = require("../services/promotion.services");

const {
  STATUS_CODE,
} = require("../constants/base.constants");

class PromotionController extends BaseCrudController {
  constructor() {
    super(service);
  }

  createOrUpdate = async (req, res, next) => {
    let id = req.params.id.trim();
    const matchingConditions = { _id: id };
    let dataUpdate = req.body;
    dataUpdate['icon'] = dataUpdate['icon'] ? dataUpdate['icon'] : null;

    const signalResponse = await service.createOrUpdate(
      matchingConditions,
      dataUpdate
    );
    return res
      .status(signalResponse.error ? STATUS_CODE.BAD_REQUEST : STATUS_CODE.OK)
      .json(signalResponse);
  };
}

module.exports = new PromotionController();
